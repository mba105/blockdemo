using UnityEngine;
using System.Collections;

public class LevelChange : MonoBehaviour
{
	public string scoreScene = "";

	private CameraFade fader;

	private Color blueSolid = new Color32(49, 77, 121, 255);
	private Color blueClear = new Color32(49, 77, 121, 0);

	private void Awake()
	{
		StartFadeIn();
	}

	private void StartFadeIn()
	{
		fader = CameraFade.CreatePersistent(blueSolid, blueSolid, 2.0f, FadeIn);
	}
	private void FadeIn()
	{
		fader.StartFade(blueSolid, blueClear, 2.0f, false, EndFadeIn);
	}
	private void EndFadeIn()
	{
		Destroy(fader.gameObject);
	}


	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
			fader = CameraFade.CreatePersistent(blueClear, blueSolid, 3.0f, EndFadeOut);
	}

	private void EndFadeOut()
	{
		if (scoreScene != "")
			Application.LoadLevel(scoreScene);
	}
}
