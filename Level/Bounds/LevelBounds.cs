using UnityEngine;
using System.Collections;

public class LevelBounds : MonoBehaviour
{
	GameObject[] checkPoints;

	void OnTriggerExit(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			collider.gameObject.GetComponent<PlayerDeath>().Kill(collider.gameObject);
		}
		else if (collider.gameObject.tag == "DangerProjectile")
		{
			Destroy(collider.gameObject);
		}
	}
}
