using UnityEngine;
using System.Collections;

public class DangerProjectile : MonoBehaviour
{
	public GameObject createEffect;
	public GameObject destroyEffect;

	private GameObject parent;

	private void Start()
	{
		SoundEffect.Create(createEffect, transform.position);
	}

	private void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			collider.gameObject.GetComponent<PlayerDeath>().Kill(collider.gameObject);

			SoundEffect.Create(destroyEffect, transform.position);
			Destroy(gameObject);
		}

		if (collider.gameObject.tag == "Terrain")
		{
			SoundEffect.Create(destroyEffect, transform.position);
			Destroy(gameObject);
		}
	}

	public Vector3 speed = Vector3.up;

	private void Update()
	{
		transform.position += speed * Time.deltaTime;
	}
}
