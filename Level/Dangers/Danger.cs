using UnityEngine;
using System.Collections;

public class Danger : MonoBehaviour
{
	GameObject[] checkPoints;

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			collider.gameObject.GetComponent<PlayerDeath>().Kill(collider.gameObject);
		}
	}
}
