using UnityEngine;
using System.Collections;

public class DangerProjectileEmitter : MonoBehaviour
{

	public GameObject projectile;
	public float maxEmissionFrequency = 2.0f;
	public float minEmissionFrequency = 6.0f;

	private float nextEmission = 0.0f;

	private GameObject parent;

	private void Start()
	{
		SetFirstEmission();

		parent = GameObject.Find("Level/Projectiles");
	}

	private void OnEnable()
	{
		SetFirstEmission();
	}

	private void DoEmission()
	{
		if (projectile != null)
		{
			GameObject p = GameObject.Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;

			if (parent != null)
				p.transform.parent = parent.transform;
		}
	}

	private void SetFirstEmission()
	{
		nextEmission = Time.time + Random.Range(0.0f, minEmissionFrequency);
	}

	private void SetNextEmission()
	{
		nextEmission = Time.time + Random.Range(maxEmissionFrequency, minEmissionFrequency);
	}

	private void Update()
	{
		if (Time.time > nextEmission)
		{
			DoEmission();
			SetNextEmission();
		}
	}
}
