Shader "Custom/Block Transparent Ambient"
{
	Properties
	{
		_MaterialColor("Block Color", Color) = (0.75, 0.75, 0.75, 1.0)
		
		_MainLightDir("Main Light Dir", Vector) = (0.0, -1.0, 0.0, 0.0)
		_MainLightColor("Main Light Color", Color) = (1.0, 1.0, 1.0, 1.0)
		
		_ALightColor ("Ambient Light Color", Color) = (0.0, 0.0, 0.0, 1.0)
		_AOColor ("AO Color", Color) = (0.0, 0.0, 0.0, 1.0)
		_AOIntensity ("AO Intensity", Range(0.0, 1.0)) = 1.0
		_AOPower ("AO Power", Range(1.0, 10.0)) = 1.0
	}
	
	SubShader
	{
		Tags { "Queue" = "Transparent" "IgnoreProjector"="True" "RenderType" = "Transparent"  }
		
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask RGBA
		
		LOD 200
		
		CGPROGRAM
		
		#pragma surface surf Lambert alpha noambient
		
		struct Input
		{
			float4 color : COLOR;
		};
		
		float4 _MaterialColor;
		float4 _MainLightDir;
		float4 _MainLightColor;
		
		half4 _ALightColor;
		half4 _AOColor;
		float _AOIntensity;
		float _AOPower;
		
		half3 dirLight(float3 lightDir, float4 lightColor, float3 surfColor, float3 surfNormal)
		{
			// Probably world space...
			// But who'd bother documenting something useful like that?
			
			half diff = max(0.0, dot(surfNormal, normalize(lightDir)));
			
			half4 result = half4(0.0);
			result.rgb = surfColor * lightColor.rgb * diff * 2.0;
			
			return result.rgb * lightColor.a;
		}
		
		void surf(Input IN, inout SurfaceOutput o)
		{
			o.Albedo = _MaterialColor.rgb;
			o.Alpha  = _MaterialColor.a;
			
			half ao = pow((1.0 - IN.color.a) * _AOIntensity, _AOPower);
			
			o.Emission = 
				lerp(_ALightColor, _AOColor, ao) +
				dirLight(_MainLightDir.xyz, _MainLightColor, _MaterialColor.rgb, o.Normal);
		}
		ENDCG
	} 
	FallBack "Transparent/Diffuse"
}
