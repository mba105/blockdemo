using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
	public string sceneName = "";

	public GUISkin skin;

	private AsyncOperation async;

	private void Start()
	{
		StartCoroutine(LoadScene());
	}

	IEnumerator LoadScene()
	{
		if (sceneName == "")
			yield break;

		async = Application.LoadLevelAsync(sceneName);

		yield return async;
	}

	private void OnGUI()
	{
		GUI.skin = skin;

		// Begin push to center
		GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();

		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();


		// Text
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label(new GUIContent("Loading...")); 
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		// Progress bar
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		if (async != null)
		{
			float progress = Mathf.Clamp01(async.progress);

			int fullBarLength = Screen.width / 2;
			int currentBarLength = (int) Mathf.Max(28.0f, fullBarLength * progress);

			GUILayout.BeginHorizontal(
				GUILayout.MinWidth(fullBarLength),
				GUILayout.MaxWidth(fullBarLength));

			GUILayout.Box(new GUIContent("", ""), 
				GUILayout.MinWidth(currentBarLength), 
				GUILayout.MaxWidth(currentBarLength));

			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();


		// End push to center
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();

		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		GUILayout.EndArea();


	}
}
