using UnityEngine;
using System.Collections;

public class MovingLighting : MonoBehaviour
{

	private GlobalLighting lighting;
	private MeshLighting meshLighting;

	private void Start()
	{
		meshLighting = gameObject.GetComponent<MeshLighting>();

		if (meshLighting == null)
			return;


		GameObject globalLighting = GameObject.FindGameObjectWithTag("MainLight");

		if (globalLighting == null)
			return;


		lighting = globalLighting.GetComponent<GlobalLighting>();
	}

	private void Update()
	{
		if (lighting == null)
			return;

		if (meshLighting == null)
			return;

		float value = lighting.GetLightingAt(transform.position.x, transform.position.y);

		meshLighting.dLightContribution = value;
	}
}
