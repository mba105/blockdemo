using UnityEngine;
using System.Collections;

public class PointInMesh : MonoBehaviour
{
	public static bool Test(Vector3 point, Collider collider)
	{
		if (!collider.bounds.Contains(point))
			return false;

		Vector3 outsidePoint = point + new Vector3(0.0f, collider.bounds.size.y, 0.0f);

		int hits = ProgressiveRaycast(outsidePoint, Vector3.down, collider.bounds.size.y, collider);
		int antiHits = ProgressiveRaycast(point, Vector3.up, collider.bounds.size.y, collider);

		return ((hits + antiHits) % 2 != 0);
	}


	private static float epsilon = 0.01f;

	private static int ProgressiveRaycast(Vector3 origin, Vector3 direction, float distance, Collider collider)
	{
		if (distance <= 0.0f)
			return 0;

		RaycastHit hit;
		if (collider.Raycast(new Ray(origin, direction), out hit, distance))
			return 1 + ProgressiveRaycast(hit.point + direction * epsilon, direction, distance - hit.distance, collider);

		return 0;
	}
}
