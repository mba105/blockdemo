using UnityEngine;
using System.Collections;

public class SkinnedMeshLighting : MonoBehaviour
{
	public bool ambient = true;

	[HideInInspector]
	public float lookupScale = 1.0f;

	public float dLightContribution = 1.0f;

	private void Start()
	{
		// Get property block
		GameObject mainLight = GameObject.FindGameObjectWithTag("MainLight");

		if (mainLight == null)
			return;


		GlobalLighting lighting = mainLight.GetComponent<GlobalLighting>();

		if (lighting == null)
			return;

		// Get material
		if (renderer == null)
			return;

		if (renderer as SkinnedMeshRenderer == null)
			return;

		Material material = renderer.material;

		lighting.SetMaterialProperties(material, dLightContribution, ambient);
	}
}
