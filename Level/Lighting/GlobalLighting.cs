using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class GlobalLighting : MonoBehaviour
{
	public Light directionalLight;
	public Material controlMaterial;

	public Texture2D lightingImage;

	public MaterialPropertyBlock dProperties, aProperties;

	static private string[] shaderNames = new string[]
	{
		"Custom/Block Ambient",
		"Custom/Block Transparent Ambient",
	};

	private string dLightDirString = "_MainLightDir";
	private int dLightDirID;
	private string dLightColorString = "_MainLightColor";
	private int dLightColorID;
	private string aLightColorString = "_ALightColor";
	private int aLightColorID;
	private string aoColorString = "_AOColor";
	private int aoColorID;
	private string aoIntensityString = "_AOIntensity";
	private int aoIntensityID;
	private string aoPowerString = "_AOPower";
	private int aoPowerID;

	private Vector4 dLightDir;
	private Color dLightColor;
	private Color aLightColor;
	private Color aoColor;
	private float aoIntensity;
	private float aoPower;

	private Color[] lightingPixels;

	private void Awake()
	{
		if (directionalLight == null)
			return;

		if (directionalLight.type != LightType.Directional)
			return;

		if (controlMaterial == null)
			return;

		Shader shader = controlMaterial.shader;

		if (shader == null)
			return;
		
		if (!shaderNames.Contains(controlMaterial.shader.name))
			return;

		// Get global ids for filling property block quicker.
		dLightDirID = Shader.PropertyToID(dLightDirString);
		dLightColorID = Shader.PropertyToID(dLightColorString);
		aLightColorID = Shader.PropertyToID(aLightColorString);
		aoColorID = Shader.PropertyToID(aoColorString);
		aoIntensityID = Shader.PropertyToID(aoIntensityString);
		aoPowerID = Shader.PropertyToID(aoPowerString);

		// Set values for global properties.
		dLightDir = -directionalLight.transform.TransformDirection(Vector3.forward);
		dLightDir.w = 0.0f;
		dLightColor = directionalLight.color;
		dLightColor.a = directionalLight.intensity;

		aLightColor = controlMaterial.GetColor(aLightColorString);
		aoColor = controlMaterial.GetColor(aoColorString);
		aoIntensity = controlMaterial.GetFloat(aoIntensityString);
		aoPower = controlMaterial.GetFloat(aoPowerString);

		// Setup property blocks.
		dProperties = new MaterialPropertyBlock();
		aProperties = new MaterialPropertyBlock();

		SetupDProperties(1.0f);
		SetupAProperties(1.0f);

		if (lightingImage != null)
			lightingPixels = lightingImage.GetPixels();
	}

	public void SetupDProperties(float dLightContribution)
	{
		dProperties.Clear();

		Color alteredColor = dLightColor;
		alteredColor.a = dLightContribution;

		dProperties.AddVector(dLightDirID, dLightDir);
		dProperties.AddColor(dLightColorID, alteredColor);
	}

	public void SetupAProperties(float dLightContribution)
	{
		aProperties.Clear();

		Color alteredColor = dLightColor;
		alteredColor.a = dLightContribution;

		aProperties.AddVector(dLightDirID, dLightDir);
		aProperties.AddColor(dLightColorID, alteredColor);

		aProperties.AddColor(aLightColorID, aLightColor);
		aProperties.AddColor(aoColorID, aoColor);
		aProperties.AddFloat(aoIntensityID, aoIntensity);
		aProperties.AddFloat(aoPowerID, aoPower);
	}

	public void SetMaterialProperties(Material material, float dLightContribution, bool ambient)
	{
		Color alteredColor = dLightColor;
		alteredColor.a = dLightContribution;

		material.SetVector(dLightDirString, dLightDir);
		material.SetColor(dLightColorString, alteredColor);

		if (ambient)
		{
			material.SetColor(aLightColorString, aLightColor);
			material.SetColor(aoColorString, aoColor);
			material.SetFloat(aoIntensityString, aoIntensity);
			material.SetFloat(aoPowerString, aoPower);
		}
	}

	public float GetLightingAt(float x, float y)
	{
		if (lightingImage == null)
			return 1.0f;

		if (x < 0.0f || x >= lightingImage.width ||
			y < 0.0f || y >= lightingImage.height)
			return 1.0f;

		int xUp = Mathf.CeilToInt(x);
		int xDown = (int)x;

		int yUp = Mathf.CeilToInt(y);
		int yDown = (int)y;

		int xUpyUp = yUp * lightingImage.width + xUp;
		int xUpyDown = yDown * lightingImage.width + xUp;
		int xDownyUp = yUp * lightingImage.width + xDown;
		int xDownyDown = yDown * lightingImage.width + xDown;

		if (xUpyUp >= lightingPixels.Length || xUpyDown >= lightingPixels.Length ||
			xDownyUp >= lightingPixels.Length || xDownyUp >= lightingPixels.Length)
			return 1.0f;

		float xDownValue = Mathf.Lerp(
			lightingPixels[xDownyDown].grayscale,
			lightingPixels[xDownyUp].grayscale,
			y - yDown);
		float xUpValue = Mathf.Lerp(
			lightingPixels[xUpyDown].grayscale,
			lightingPixels[xUpyUp].grayscale,
			y - yDown);

		float result = Mathf.Lerp(xDownValue, xUpValue, x - xDown);

		return Mathf.Clamp01(result);
	}
}
