using UnityEngine;
using System.Collections;

public class MeshLighting : MonoBehaviour
{
	public bool ambient = true;

	[HideInInspector]
	public float lookupScale = 1.0f;

	public float dLightContribution = 1.0f;

	private Material material;
	private Mesh mesh;

	private MaterialPropertyBlock propertyBlock;

	private GlobalLighting lighting;

	private void Start()
	{
		// Get property block
		GameObject mainLight = GameObject.FindGameObjectWithTag("MainLight");

		if (mainLight == null)
			return;


		lighting = mainLight.GetComponent<GlobalLighting>();

		if (lighting == null)
			return;

		if (ambient)
			propertyBlock = lighting.aProperties;
		else
			propertyBlock = lighting.dProperties;


		// Get mesh
		MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();

		if (meshFilter == null)
			return;

		mesh = meshFilter.sharedMesh;

		if (mesh == null)
			return;


		// Get material
		if (renderer == null)
			return;

		material = renderer.material;


		// Disable standard rendering
		renderer.enabled = false;
	}

	private void Update()
	{
		if (ambient)
			lighting.SetupAProperties(dLightContribution);
		else
			lighting.SetupDProperties(dLightContribution);

		Graphics.DrawMesh(
			mesh, transform.localToWorldMatrix, material, 
			0, null, 0, // layer, camera, submesh
			propertyBlock, 
			true, true); // cast / receive shadows
	}
}
