using UnityEngine;
using System.Collections;

public class SoundEffect : MonoBehaviour
{
	private static GameObject parent;

	public static void Create(GameObject prefab, Vector3 position)
	{
		if (prefab == null)
			return;

		GameObject effect = GameObject.Instantiate(prefab, position, Quaternion.identity) as GameObject;

		if (parent == null)
			parent = GameObject.Find("Level/Effects");

		if (parent != null)
			effect.transform.parent = parent.transform;
	}

	private void Start()
	{
		audio.Play();
	}

	private void Update()
	{
		if (!audio.isPlaying)
			Destroy(gameObject);
	}
}
