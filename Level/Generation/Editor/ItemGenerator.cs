using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Zip;

public class ItemGenerator
{
	private static bool EqualColors(float t1, float t2)
	{
		float epsilon = 1e-3f;
		return Mathf.Abs(t1 - t2) < epsilon;
	}

	private bool EqualColors(Color c1, Color c2)
	{
		return
			EqualColors(c1.r, c2.r) &&
			EqualColors(c1.g, c2.g) &&
			EqualColors(c1.b, c2.b) &&
			EqualColors(c1.a, c2.a);
	}

	private void CreateItems(GameObject root, Color[] pixels, Dictionary<Color, GameObject> imageKey, int width, int height)
	{
		Dictionary<Color, string> errors = new Dictionary<Color, string>();

		for (int y = 0; y != height; ++y)
		{
			for (int x = 0; x != width; ++x)
			{
				int index = y * width + x;

				Color pixel = pixels[index];

				if (Mathf.Approximately(pixel.a, 0.0f))
					continue;

				try
				{
					KeyValuePair<Color, GameObject> key = imageKey.First(k => EqualColors(k.Key, pixel));

					if (key.Value == null)
					{
						if (!errors.ContainsKey(pixel))
							errors.Add(pixel, "Invalid object for item color key: " + pixels[index].ToString());

						continue;
					}

					GameObject block = GameObject.Instantiate(key.Value) as GameObject;
					block.transform.parent = root.transform;
					block.transform.position = new Vector3(x + 0.5f, y + 0.5f, 0.0f);
				}
				catch (InvalidOperationException)
				{
					if (!errors.ContainsKey(pixel))
						errors.Add(pixel, "Item color key not found in map: " + pixels[index].ToString());

					continue;
				}

				EditorUtility.DisplayProgressBar(
					"Item Generation",
					"" + index + " / " + pixels.Length,
					(float)index / (float)pixels.Length);
			}
		}

		EditorUtility.ClearProgressBar();

		foreach (string message in errors.Values)
			UnityEngine.Debug.LogWarning(message);
	}

	public void Generate(GameObject levelRoot, Texture2D image, Color[] keyColors, GameObject[] keyObjects)
	{
		if (image == null)
		{
			UnityEngine.Debug.LogError("Item image texture is null! Unable to generate items!");
			return;
		}

		if (image.width == 0 || image.height == 0)
		{
			UnityEngine.Debug.LogError("Item image size 0! Unable to generate items!");
		}

		Dictionary<Color, GameObject> itemKey =
			keyColors.Zip(keyObjects, (k, v) => new { k, v })
				.ToDictionary(x => x.k, x => x.v);

		if (itemKey.Count == 0)
		{
			UnityEngine.Debug.LogError("Item image key is empty! Unable to generate items!");
			return;
		}


		GameObject itemsObject = GameObjectRelations.FirstChild(levelRoot, "Items");
		UnityEngine.Object.DestroyImmediate(itemsObject);

		itemsObject = new GameObject("Items");
		itemsObject.transform.parent = levelRoot.transform;


		Color[] pixels = image.GetPixels();

		Stopwatch stopwatch = Stopwatch.StartNew();

		CreateItems(itemsObject, pixels, itemKey, image.width, image.height);

		string output = "";
		output += "Item generation complete!\n";
		output += stopwatch.ElapsedMilliseconds + "ms";

		UnityEngine.Debug.Log(output);
	}
}
