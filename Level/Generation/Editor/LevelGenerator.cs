using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelGenerator : ScriptableObject
{
	public Texture2D terrainImage;
	public int terrainBlockSize = 12;

	// BUG (workaround):
	// Unity doesn't serialize dictionaries.
	public Color[] terrainKeyColors = new Color[0];
	public GameObject[] terrainKeyObjects = new GameObject[0];

	public int backgroundBlockSize = 12;
	public Texture2D[] backgroundImages = new Texture2D[0];
	public float[] backgroundOffsets = new float[0];
	public float[] backgroundScales = new float[0];

	// BUG (workaround):
	// Unity doesn't serialize dictionaries.
	public Color[] backgroundKeyColors = new Color[0];
	public GameObject[] backgroundKeyObjects = new GameObject[0];


	public Texture2D itemImage;

	// BUG (workaround):
	// Unity doesn't serialize dictionaries.
	public Color[] itemKeyColors = new Color[0];
	public GameObject[] itemKeyObjects = new GameObject[0];


	public Texture2D lightingImage;

	private GameObject FindRoot()
	{
		GameObject root = GameObject.Find("GeneratedLevel");

		if (root == null)
			root = new GameObject("GeneratedLevel");

		return root;
	}

	public void GenerateAll()
	{
		ClearAll();

		GenerateTerrain();
		GenerateBackground();
		GenerateItems();
		GenerateLighting();
	}
	
	public void GenerateTerrain()
	{
		TerrainGenerator terrainGenerator = new TerrainGenerator();
		terrainGenerator.Generate(FindRoot(), terrainImage, terrainKeyColors, terrainKeyObjects, terrainBlockSize);
	}

	public void GenerateBackground()
	{
		BackgroundGenerator backgroundGenerator = new BackgroundGenerator();

		for (int i = 0; i != backgroundImages.Length; ++i)
		{
			backgroundGenerator.Generate(FindRoot(), backgroundImages[i],
				backgroundKeyColors, backgroundKeyObjects,
				backgroundBlockSize, backgroundOffsets[i], backgroundScales[i],
				i == 0 ? true : false);
		}
	}

	public void GenerateItems()
	{
		ItemGenerator itemGenerator = new ItemGenerator();
		itemGenerator.Generate(FindRoot(), itemImage, itemKeyColors, itemKeyObjects);
	}

	public void GenerateLighting()
	{
		LightingGenerator lightingGenerator = new LightingGenerator();
		lightingGenerator.Generate(FindRoot(), lightingImage);
	}

	public void ClearAll()
	{
		GameObject levelRoot = FindRoot();

		if (levelRoot != null)
		{
			if (Application.isPlaying)
				Destroy(levelRoot);
			else
				DestroyImmediate(levelRoot);
		}
	}
}
