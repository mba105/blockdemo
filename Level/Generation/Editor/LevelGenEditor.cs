using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(LevelGenerator))]
public class LevelGenEditor : Editor
{
	private static bool terrainKeyFoldout = true;
	private static bool backgroundLayerFoldout = true;
	private static bool backgroundKeyFoldout = true;
	private static bool itemKeyFoldout = true;

	private LevelGenerator targetSettings;
	private SerializedObject serializedSettings;

	private void OnEnable()
	{
		targetSettings = (target as LevelGenerator);
		serializedSettings = new SerializedObject(targetSettings);
	}

	public override void OnInspectorGUI()
	{
		serializedSettings.Update();

		// Standard width (100px) is too small.
		EditorGUIUtility.LookLikeControls(130.0f);

		EditorGUILayout.Separator();

		if (GUILayout.Button("Generate All!"))
			targetSettings.GenerateAll();

		if (GUILayout.Button("Generate Terrain!"))
			targetSettings.GenerateTerrain();

		if (GUILayout.Button("Generate Background!"))
			targetSettings.GenerateBackground();

		if (GUILayout.Button("Generate Items!"))
			targetSettings.GenerateItems();

		if (GUILayout.Button("Generate Lighting!"))
			targetSettings.GenerateLighting();

		if (GUILayout.Button("Clear All!"))
			targetSettings.ClearAll();

		EditorGUILayout.Separator();

		EditorGUILayout.PropertyField(serializedSettings.FindProperty("terrainImage"),
			new GUIContent("Terrain Image", "Image for terrain generation."));

		EditorGUILayout.PropertyField(serializedSettings.FindProperty("terrainBlockSize"),
			new GUIContent("Terrain Block Size", "Image is divided into blocks of this size for processing."));
		
		if (terrainKeyFoldout = EditorGUILayout.Foldout(terrainKeyFoldout, "Terrain Key"))
		{
			Indent();

			// BUG (workaround):
			// PropertyField doesn't work for dictionaries / arrays / lists.
			// Attempting to iterate through also doesn't work.
			// So we have to do this manually (and apply / update the serialized property before / after).

			EditorUtility.SetDirty(targetSettings); // ?
			serializedSettings.ApplyModifiedProperties();

			int oldKeyCount = targetSettings.terrainKeyColors.Length;
			int newKeyCount = EditorGUILayout.IntField("Size", oldKeyCount);

			if (newKeyCount != oldKeyCount)
			{
				Array.Resize(ref targetSettings.terrainKeyColors, newKeyCount);
				Array.Resize(ref targetSettings.terrainKeyObjects, newKeyCount);
			}

			for (int i = 0; i != newKeyCount; ++i)
			{
				EditorGUILayout.BeginHorizontal();

				Color oldColor = targetSettings.terrainKeyColors[i];
				Color newColor = EditorGUILayout.ColorField(oldColor);

				if (newColor != oldColor)
					targetSettings.terrainKeyColors[i] = newColor;

				GameObject oldBlock = targetSettings.terrainKeyObjects[i];
				GameObject newBlock = EditorGUILayout.ObjectField(oldBlock, typeof(GameObject), false) as GameObject;

				if (newBlock != oldBlock)
					targetSettings.terrainKeyObjects[i] = newBlock;

				EditorGUILayout.EndHorizontal();
			}

			serializedSettings.Update();

			UnIndent();
		}

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		if (backgroundLayerFoldout = EditorGUILayout.Foldout(backgroundLayerFoldout, "Background Layer"))
		{
			Indent();

			// BUG (workaround):
			// PropertyField doesn't work for dictionaries / arrays / lists.
			// Attempting to iterate through also doesn't work.
			// So we have to do this manually (and apply / update the serialized property before / after).

			EditorUtility.SetDirty(targetSettings); // ?
			serializedSettings.ApplyModifiedProperties();

			int oldLayerCount = targetSettings.backgroundImages.Length;
			int newLayerCount = EditorGUILayout.IntField("Size", oldLayerCount);

			if (newLayerCount != oldLayerCount)
			{
				Array.Resize(ref targetSettings.backgroundImages, newLayerCount);
				Array.Resize(ref targetSettings.backgroundOffsets, newLayerCount);
				Array.Resize(ref targetSettings.backgroundScales, newLayerCount);
			}

			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));
			GUILayout.Label("Image");
			GUILayout.Label("Offset");
			GUILayout.Label("Scale");
			EditorGUILayout.EndHorizontal();

			for (int i = 0; i != newLayerCount; ++i)
			{
				EditorGUILayout.BeginHorizontal();

				Texture2D oldImage = targetSettings.backgroundImages[i];
				Texture2D newImage = EditorGUILayout.ObjectField(oldImage, typeof(Texture2D), false) as Texture2D;

				if (newImage != oldImage)
					targetSettings.backgroundImages[i] = newImage;

				float oldOffset = targetSettings.backgroundOffsets[i];
				float newOffset = EditorGUILayout.FloatField(oldOffset);

				if (newOffset != oldOffset)
					targetSettings.backgroundOffsets[i] = newOffset;

				float oldScale = targetSettings.backgroundScales[i];
				float newScale = EditorGUILayout.FloatField(oldScale);

				if (newScale != oldScale)
					targetSettings.backgroundScales[i] = newScale;

				EditorGUILayout.EndHorizontal();
			}

			serializedSettings.Update();

			UnIndent();
		}

		EditorGUILayout.PropertyField(serializedSettings.FindProperty("backgroundBlockSize"),
			new GUIContent("Background Block Size", "Image is divided into blocks of this size for processing."));

		if (backgroundKeyFoldout = EditorGUILayout.Foldout(backgroundKeyFoldout, "Background Key"))
		{
			Indent();

			// BUG (workaround):
			// PropertyField doesn't work for dictionaries / arrays / lists.
			// Attempting to iterate through also doesn't work.
			// So we have to do this manually (and apply / update the serialized property before / after).

			EditorUtility.SetDirty(targetSettings); // ?
			serializedSettings.ApplyModifiedProperties();

			int oldKeyCount = targetSettings.backgroundKeyColors.Length;
			int newKeyCount = EditorGUILayout.IntField("Size", oldKeyCount);

			if (newKeyCount != oldKeyCount)
			{
				Array.Resize(ref targetSettings.backgroundKeyColors, newKeyCount);
				Array.Resize(ref targetSettings.backgroundKeyObjects, newKeyCount);
			}

			for (int i = 0; i != newKeyCount; ++i)
			{
				EditorGUILayout.BeginHorizontal();

				Color oldColor = targetSettings.backgroundKeyColors[i];
				Color newColor = EditorGUILayout.ColorField(oldColor);

				if (newColor != oldColor)
					targetSettings.backgroundKeyColors[i] = newColor;

				GameObject oldBlock = targetSettings.backgroundKeyObjects[i];
				GameObject newBlock = EditorGUILayout.ObjectField(oldBlock, typeof(GameObject), false) as GameObject;

				if (newBlock != oldBlock)
					targetSettings.backgroundKeyObjects[i] = newBlock;

				EditorGUILayout.EndHorizontal();
			}

			serializedSettings.Update();

			UnIndent();
		}

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		EditorGUILayout.PropertyField(serializedSettings.FindProperty("itemImage"),
			new GUIContent("Item Image", "Image for item creation."));

		if (itemKeyFoldout = EditorGUILayout.Foldout(itemKeyFoldout, "Item Key"))
		{
			Indent();

			// BUG (workaround):
			// PropertyField doesn't work for dictionaries / arrays / lists.
			// Attempting to iterate through also doesn't work.
			// So we have to do this manually (and apply / update the serialized property before / after).

			EditorUtility.SetDirty(targetSettings); // ?
			serializedSettings.ApplyModifiedProperties();

			int oldKeyCount = targetSettings.itemKeyColors.Length;
			int newKeyCount = EditorGUILayout.IntField("Size", oldKeyCount);

			if (newKeyCount != oldKeyCount)
			{
				Array.Resize(ref targetSettings.itemKeyColors, newKeyCount);
				Array.Resize(ref targetSettings.itemKeyObjects, newKeyCount);
			}

			for (int i = 0; i != newKeyCount; ++i)
			{
				EditorGUILayout.BeginHorizontal();

				Color oldColor = targetSettings.itemKeyColors[i];
				Color newColor = EditorGUILayout.ColorField(oldColor);

				if (newColor != oldColor)
					targetSettings.itemKeyColors[i] = newColor;

				GameObject oldBlock = targetSettings.itemKeyObjects[i];
				GameObject newBlock = EditorGUILayout.ObjectField(oldBlock, typeof(GameObject), false) as GameObject;

				if (newBlock != oldBlock)
					targetSettings.itemKeyObjects[i] = newBlock;

				EditorGUILayout.EndHorizontal();
			}

			serializedSettings.Update();

			UnIndent();
		}

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		EditorGUILayout.PropertyField(serializedSettings.FindProperty("lightingImage"),
			new GUIContent("Lighting Image", "Image for lighting."));

		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		// BUG (existing):
		// When this inspector is active, Unity always thinks the object is dirty. This causes problems with saving.
		// Unless GUI.changed worked differently or the FooField functions all return bools there is no simple workaround.
		// Note SetDirty is also used above.
		EditorUtility.SetDirty(targetSettings);

		serializedSettings.ApplyModifiedProperties();
	}


	// BUG (workaround):
	// EditorGUI.indentLevel doesn't work properly on object fields, vectors, etc.
	// Doing it this way is consistent and allows control of actual indent width.
	private void Indent()
	{
		EditorGUILayout.BeginHorizontal();
		GUILayout.Space(15.0f);

		EditorGUILayout.BeginVertical();
	}

	private void UnIndent()
	{
		EditorGUILayout.EndVertical();
		EditorGUILayout.EndHorizontal();
	}
}
