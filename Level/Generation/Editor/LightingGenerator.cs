using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class LightingGenerator
{
	public void Generate(GameObject levelRoot, Texture2D image)
	{
		MeshLighting[] meshLightings = Object.FindSceneObjectsOfType(
			typeof(MeshLighting)) as MeshLighting[];

		SkinnedMeshLighting[] skinnedMeshLightings = Object.FindSceneObjectsOfType(
			typeof(SkinnedMeshLighting)) as SkinnedMeshLighting[];

		Color[] pixels = image.GetPixels();

		int lightCount = 0;
		int totalMeshLights = meshLightings.Length + skinnedMeshLightings.Length;

		foreach (MeshLighting ml in meshLightings)
		{
			EditorUtility.DisplayProgressBar(
				"Lighting Generation",
				++lightCount + " / " + totalMeshLights + " blocks",
				(float)lightCount / (float)totalMeshLights);

			Transform transform = ml.gameObject.transform;

			int index =
				(int)(transform.position.y * ml.lookupScale) * image.width + 
				(int)(transform.position.x * ml.lookupScale);

			ml.dLightContribution = pixels[index].grayscale;
		}

		foreach (SkinnedMeshLighting sml in skinnedMeshLightings)
		{
			EditorUtility.DisplayProgressBar(
				"Lighting Generation",
				++lightCount + " / " + totalMeshLights + " blocks",
				(float)lightCount / (float)totalMeshLights);

			Transform transform = sml.gameObject.transform;

			int index =
				(int)(transform.position.y * sml.lookupScale) * image.width +
				(int)(transform.position.x * sml.lookupScale);

			sml.dLightContribution = pixels[index].grayscale;
		}

		EditorUtility.ClearProgressBar();
	}
}
