using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MinElement;
using UnityEngine;
using UnityEditor;
using Zip;

public class BackgroundGenerator
{

	private static bool EqualColors(float t1, float t2)
	{
		float epsilon = 1e-3f;
		return Mathf.Abs(t1 - t2) < epsilon;
	}

	private bool EqualColors(Color c1, Color c2)
	{
		return
			EqualColors(c1.r, c2.r) &&
			EqualColors(c1.g, c2.g) &&
			EqualColors(c1.b, c2.b) &&
			EqualColors(c1.a, c2.a);
	}

	private int FindEqualArea(Color[] pixels, Color color, int i, int j, int width, int height)
	{
		int size = 1;

		while (j + size != height && i + size != width)
		{
			for (int y = j + size, x = i; x != i + size + 1; ++x)
			{
				int index2 = y * width + x;

				if (!EqualColors(color, pixels[index2]))
					return size;
			}

			for (int x = i + size, y = j; y != j + size + 1 - 1; ++y)
			{
				int index2 = y * width + x;

				if (!EqualColors(color, pixels[index2]))
					return size;
			}

			++size;
		}

		return size;
	}

	private int[] FindMinRuns(Color[] pixels, int width, int height)
	{
		int[] runs = new int[pixels.Length];

		for (int j = 0; j != height; ++j)
		{
			for (int i = 0; i != width; ++i)
			{
				int index = j * width + i;

				Color color = pixels[index];

				if (Mathf.Approximately(color.a, 0.0f))
				{
					runs[index] = 0;
					continue;
				}

				runs[index] = FindEqualArea(pixels, color, i, j, width, height);
			}
		}

		return runs;
	}

	private class Box
	{
		public Box(int xOrigin, int yOrigin, int size)
		{
			XOrigin = xOrigin;
			YOrigin = yOrigin;

			Size = size;
		}

		public int XOrigin;
		public int YOrigin;

		public int Size;

		public int[] Coverage(int width, int height)
		{
			var xRange = Enumerable.Range(XOrigin, Size);
			var yRange = Enumerable.Range(YOrigin, Size);

			var cartesian =
				from y in yRange
				from x in xRange
				select y * width + x;

			return cartesian.ToArray();
		}

		public override string ToString()
		{
			string s = "Box: "
				+ "(" + XOrigin.ToString() + ", " + YOrigin.ToString() + ")"
				+ " " + Size.ToString();
			return s;
		}

		public bool Overlaps(Box b2)
		{
			if (XOrigin >= b2.XOrigin + b2.Size)
				return false;
			if (b2.XOrigin >= XOrigin + Size)
				return false;
			if (YOrigin >= b2.YOrigin + b2.Size)
				return false;
			if (b2.YOrigin >= YOrigin + Size)
				return false;

			return true;
		}
	}

	private List<Box> FindPossibleBoxes(int[] minRuns, int width, int height)
	{
		List<Box> choiceList = new List<Box>();

		for (int y = 0; y != height; ++y)
		{
			for (int x = 0; x != width; ++x)
			{
				int index = y * width + x;

				if (minRuns[index] == 0)
					continue;

				for (int i = minRuns[index]; i != 0; --i)
					choiceList.Add(new Box(x, y, i));
			}
		}

		return choiceList;
	}

	private List<List<Box>> FindUnconnectedSets(List<Box> boxes)
	{
		var unconnectedSets = new List<List<Box>>();

		while (boxes.Any())
		{
			var set = boxes.Take(1).ToList();
			boxes = boxes.Skip(1).ToList();
			//boxes.RemoveAt(0);

			var selected = new HashSet<Box>();

			do
			{
				selected = new HashSet<Box>(
					boxes.Where(b =>
						set.Any(s =>
							s.Overlaps(b)
						)
					));

				set.AddRange(selected);
				boxes.RemoveAll(selected.Contains);
			}
			while (selected.Any());

			unconnectedSets.Add(set);
		}

		return unconnectedSets;
	}

	private List<Box> FindMinExactCover(List<List<Box>> unconnectedSets, int width, int height)
	{
		List<Box> fullSolution = new List<Box>();

		foreach (var set in unconnectedSets)
		{
			if (set.Count() == 1)
			{
				fullSolution.AddRange(set);
			}
			else
			{
				DLX.DancingLinks<Box, int> dlx = new DLX.DancingLinks<Box, int>();

				var coverage =
					from c in set
					select c.Coverage(width, height);

				var constraints =
					(from x in coverage
					 from y in x
					 select y).Distinct();

				dlx.AddConstraints(constraints);

				dlx.AddChoices(
					set.Zip(coverage,
						(ch, co) => new DLX.DancingLinks<Box, int>.ChoiceData(ch, co)));

				Box[] minSolution = dlx.SolveMin();

				fullSolution.AddRange(minSolution);
			}
		}

		return fullSolution;
	}

	private void CreateTerrain(GameObject root, Color[] pixels, Dictionary<Color, GameObject> imageKey, List<Box> boxes, int xOrigin, int yOrigin, int width, int height, float offset, float scale)
	{

		Dictionary<Color, string> errors = new Dictionary<Color, string>();

		foreach (Box b in boxes)
		{
			int index = b.YOrigin * width + b.XOrigin;

			Color pixel = pixels[index];

			try
			{
				KeyValuePair<Color, GameObject> key = imageKey.First(k => EqualColors(k.Key, pixel));

				if (key.Value == null)
				{
					if (!errors.ContainsKey(pixel))
						errors.Add(pixel, "Invalid object for terrain color key: " + pixels[index].ToString());

					continue;
				}

				GameObject block = GameObject.Instantiate(key.Value) as GameObject;
				block.transform.parent = root.transform;
				block.transform.position = new Vector3(
					(xOrigin + b.XOrigin + b.Size / 2.0f) * scale, 
					(yOrigin + b.YOrigin + b.Size / 2.0f) * scale, 
					offset);
				block.transform.localScale = new Vector3(b.Size * scale, b.Size * scale, b.Size * scale);

				MeshLighting lighting = block.GetComponent<MeshLighting>();

				if (lighting != null)
					lighting.lookupScale = scale;
			}
			catch (InvalidOperationException)
			{
				if (!errors.ContainsKey(pixel))
					errors.Add(pixel, "Background color key not found in map: " + pixels[index].ToString());

				continue;
			}
		}

		foreach (string message in errors.Values)
			UnityEngine.Debug.LogWarning(message);
	}

	private long TickStopwatch(Stopwatch stopwatch)
	{
		long time = stopwatch.ElapsedMilliseconds;

		stopwatch.Reset();
		stopwatch.Start();

		return time;
	}

	public void Generate(GameObject levelRoot, Texture2D image, Color[] keyColors, GameObject[] keyBlocks, int blockSize, float offset, float scale, bool clear)
	{
		if (image == null)
		{
			UnityEngine.Debug.LogError("Background image texture is null! Unable to generate terrain!");
			return;
		}

		if (image.width == 0 || image.height == 0)
		{
			UnityEngine.Debug.LogError("Background image size 0! Unable to generate terrain!");
		}

		Dictionary<Color, GameObject> key =
			keyColors.Zip(keyBlocks, (k, v) => new { k, v })
				.ToDictionary(x => x.k, x => x.v);

		if (key.Count == 0)
		{
			UnityEngine.Debug.LogError("Background image key is empty! Unable to generate terrain!");
			return;
		}

		GameObject backgroundObject = GameObjectRelations.FirstChild(levelRoot, "Background");
		if (clear)
			UnityEngine.Object.DestroyImmediate(backgroundObject);

		if (backgroundObject == null)
			backgroundObject = new GameObject("Background");
		backgroundObject.transform.parent = levelRoot.transform;

		int blockCount = 0;
		int boxCount = 0;

		long runsTime = 0;
		long findTime = 0;
		long setsTime = 0;
		long solveTime = 0;
		long createTime = 0;

		Stopwatch fullStopwatch = Stopwatch.StartNew();
		Stopwatch taskStopwatch = Stopwatch.StartNew();

		int totalBlocks = 
			Mathf.CeilToInt((float) image.height / (float) blockSize) *
			Mathf.CeilToInt((float) image.width  / (float) blockSize);

		for (int y = 0; y < image.height; y += blockSize)
		{
			for (int x = 0; x < image.width; x += blockSize)
			{

				int width = Mathf.Min(image.width - x, blockSize);
				int height = Mathf.Min(image.height - y, blockSize);

				Color[] pixels = image.GetPixels(x, y, width, height);

				int[] minRuns = FindMinRuns(pixels, width, height);
				runsTime += TickStopwatch(taskStopwatch);

				List<Box> choices = FindPossibleBoxes(minRuns, width, height);
				findTime += TickStopwatch(taskStopwatch);

				List<List<Box>> unconnectedSets = FindUnconnectedSets(
					choices.OrderByDescending(b => b.Size).ToList());
				setsTime += TickStopwatch(taskStopwatch);

				List<Box> bestSolution = FindMinExactCover(unconnectedSets, width, height);
				solveTime += TickStopwatch(taskStopwatch);

				boxCount += bestSolution.Count;

				CreateTerrain(backgroundObject, pixels, key, bestSolution, x, y, width, height, offset, scale);
				createTime += TickStopwatch(taskStopwatch);

				EditorUtility.DisplayProgressBar(
					"Background Generation",
					blockCount + " / " + totalBlocks + " blocks",
					(float)blockCount / (float)totalBlocks);

				++blockCount;
			}
		}

		EditorUtility.ClearProgressBar();

		string output = "";
		output += "Background generation complete!\n";
		output += blockCount + " blocks and " + boxCount + " boxes in " + fullStopwatch.ElapsedMilliseconds + "ms\n";
		output += "Finding Runs: " + runsTime + "ms\n";
		output += "Finding Boxes: " + findTime + "ms\n";
		output += "Making Sets: " + setsTime + "ms\n";
		output += "Solving: " + solveTime + "ms\n";
		output += "Background Creation: " + createTime + "ms\n";

		UnityEngine.Debug.Log(output);
	}
}
