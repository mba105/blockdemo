using UnityEngine;
using UnityEditor;
using System.Reflection;

public class GeneratorMenuItem : MonoBehaviour
{

	static private readonly string defaultAssetName = "LevelGenerator.asset";

	[MenuItem("Assets/Create/LevelGenerator")]
	static private void CreateIconizer()
	{
		string path;

		try
		{
			System.Type type = typeof(AssetDatabase);

			MethodInfo method = type.GetMethod(
				"GetUniquePathNameAtSelectedPath",
				BindingFlags.NonPublic | BindingFlags.Static);

			path = (string)method.Invoke(type, new object[] { defaultAssetName });
		}
		catch
		{
			path = AssetDatabase.GenerateUniqueAssetPath("Assets/" + defaultAssetName);
		}

		LevelGenerator generator = ScriptableObject.CreateInstance<LevelGenerator>();

		AssetDatabase.CreateAsset(generator, path);
	}
}
