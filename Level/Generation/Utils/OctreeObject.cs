using UnityEngine;
using System.Collections;

public class OctreeObject : MonoBehaviour, IOctreeObject<OctreeObject>
{
	[HideInInspector]
	public Bounds Bounds
	{
		get;
		private set;
	}

	[HideInInspector]
	public Octree<OctreeObject> Octree { get; set; }

	[HideInInspector]
	public OctreeNode<OctreeObject> OctreeNode { get; set; }

	public bool isDynamic = false;

	private Matrix4x4 oldTransform;

	public void SetBounds()
	{
		Renderer renderer = gameObject.GetComponentInChildren<Renderer>();
		Bounds bounds = renderer == null ? new Bounds() : renderer.bounds;

		this.Bounds = bounds;
	}

	private void Awake()
	{
		SetBounds();
	}

	private void Start()
	{
		if (isDynamic)
			oldTransform = transform.localToWorldMatrix;
	}

	private void Update()
	{
		if (isDynamic)
		{
			Matrix4x4 newTransform = transform.localToWorldMatrix;

			if (newTransform != oldTransform)
			{
				if (OctreeNode != null)
				{
					OctreeNode.Remove(this);
					SetBounds();
					Octree.Add(this);
				}
			}
		}
	}

	private void OnDestroy()
	{
		if (Octree != null)
			Octree.Remove(this);
	}
}
