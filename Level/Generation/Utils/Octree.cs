using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using MinElement;

public interface IOctreeObject<T> where T : IOctreeObject<T>
{
	Bounds Bounds { get; }
	Octree<T> Octree { get; set; }
	OctreeNode<T> OctreeNode { get; set; }
}

// Based on "Dynamic Irregular Octrees" paper by Joshua Shagam and Joseph Pfeiffer, Jr.
public class OctreeNode<T> where T : IOctreeObject<T>
{
	#region properties
	public bool HasChildren
	{
		get;
		private set;
	}

	public bool IsLeaf
	{
		get { return !HasChildren; }
	}

	public bool IsEmpty
	{
		get { return (objects == null || !objects.Any()); }
	}

	public int CountNodes
	{
		get
		{
			int count = 1;

			if (HasChildren)
			{
				foreach (OctreeNode<T> child in EnabledChildren)
					count += child.CountNodes;
			}

			return count;
		}
	}
	#endregion

	#region data members
	private OctreeNode<T> parent;

	private Bounds AABB;
	private bool updateAABB;

	private static readonly int objectLimit = 8;
	private static readonly float boundsEpsilon = 0.5f;

	private HashSet<T> objects;

	private bool[] planesEnabled;
	private Plane[] planes;
	private OctreeNode<T>[] children;
	#endregion

	#region constructor
	public OctreeNode(OctreeNode<T> parent)
	{
		this.parent = parent;

		updateAABB = false;
		HasChildren = false;

		objects = new HashSet<T>();

		planesEnabled = Enumerable.Repeat(false, 3).ToArray();
		planes = new Plane[3];
		children = new OctreeNode<T>[8];

		AABB = new Bounds(Vector3.zero, Vector3.zero);
	}
	#endregion

	#region public members - add
	// If at leaf node, or object straddles a plane, insert it here.
	// Otherwise add to correct child.
	public void Add(T octreeObject)
	{
		if (HasChildren)
		{
			int index = GetIndex(octreeObject.Bounds);

			if (index != -1)
			{
				children[index].Add(octreeObject);
				return;
			}
		}

		ExpandAABB(octreeObject.Bounds);
		objects.Add(octreeObject);

		octreeObject.OctreeNode = this;
	}
	#endregion

	#region public members - remove
	public bool Remove(T octreeObject)
	{
		if (objects.Contains(octreeObject))
		{
			MarkAABBForUpdate(octreeObject.Bounds);
			objects.Remove(octreeObject);

			octreeObject.OctreeNode = null;

			if (IsLeaf && IsEmpty && parent != null)
				parent.Prune();

			return true;
		}

		return false;
	}
	#endregion

	#region public members - update
	public void Update()
	{
		if (!HasChildren && objects.Count > objectLimit)
			Split();

		if (HasChildren)
		{
			foreach (OctreeNode<T> child in EnabledChildren)
				child.Update();
		}

		if (updateAABB)
		{
			Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
			Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
			AABB.SetMinMax(min, max);

			foreach (T data in objects)
				AABB.Encapsulate(data.Bounds);

			if (HasChildren)
			{
				foreach (OctreeNode<T> child in EnabledChildren)
				{
					if (!child.IsLeaf || !child.IsEmpty)
						AABB.Encapsulate(child.AABB);
				}
			}

			updateAABB = false;
		}
	}
	#endregion

	#region public members - query
	public void Query(ICollection<T> result, Bounds bounds)
	{
		if (!AABB.Intersects(bounds))
			return;

		foreach (T data in objects)
		{
			// Could leave out this test and just include the whole node.
			// (Quicker query, but rougher results).
			if (data.Bounds.Intersects(bounds))
				result.Add(data);
		}

		if (HasChildren)
		{
			foreach (OctreeNode<T> child in EnabledChildren)
				child.Query(result, bounds);
		}
	}

	public void Query(ICollection<T> result, Plane[] planes)
	{
		if (!GeometryUtility.TestPlanesAABB(planes, AABB))
			return;

		foreach (T data in objects)
		{
			if (GeometryUtility.TestPlanesAABB(planes, data.Bounds))
				result.Add(data);
		}

		if (HasChildren)
		{
			foreach (OctreeNode<T> child in EnabledChildren)
				child.Query(result, planes);
		}
	}

	public void All(ICollection<T> result)
	{
		foreach (T data in objects)
			result.Add(data);

		if (HasChildren)
		{
			foreach (OctreeNode<T> child in EnabledChildren)
				child.All(result);
		}
	}
	#endregion

	#region public members - iterator
	public IEnumerable<OctreeNode<T>> EnabledChildren
	{
		get
		{
			foreach (int index in EnabledIndices)
				yield return children[index];
		}
	}

	private IEnumerable<int> EnabledIndices
	{
		get
		{
			for (int i = 0; i != Convert.ToInt32(planesEnabled[2]) + 1; ++i)
			{
				for (int j = 0; j != Convert.ToInt32(planesEnabled[1]) + 1; ++j)
				{
					for (int k = 0; k != Convert.ToInt32(planesEnabled[0]) + 1; ++k)
					{
						int index = (i << 3 | j << 2 | k << 1) / 2;
						yield return index;
					}
				}
			}
		}
	}
	#endregion

	#region private members - aabb
	private void ExpandAABB(Bounds bounds)
	{
		AABB.Encapsulate(bounds);

		if (parent != null)
			parent.ExpandAABB(bounds);
	}

	private void MarkAABBForUpdate(Bounds bounds)
	{
		for (int i = 0; i != 3; ++i)
		{
			if (Mathf.Abs(AABB.min[i] - bounds.min[i]) < boundsEpsilon ||
				Mathf.Abs(AABB.max[i] - bounds.max[i]) < boundsEpsilon)
			{
				updateAABB = true;

				if (parent != null)
					parent.MarkAABBForUpdate(bounds);
			}
		}
	}
	#endregion

	#region private members - utils
	// Return  0 if sphere is straddling the plane.
	// Return -1 if sphere is behind the plane.
	// Return +1 if sphere is in front of the plane.
	static private int Partition(Plane plane, Bounds bounds)
	{
		// Tests sphere enclosing AABB. Could just test the box instead?
		float distance = plane.GetDistanceToPoint(bounds.center);

		if (Mathf.Abs(distance) <= bounds.extents.magnitude)
			return 0;

		return (int)Mathf.Sign(distance);
	}

	// Return -1 if sphere is straddling any plane.
	// Return index of child node otherwise.
	private int GetIndex(Bounds bounds)
	{
		int index = 0;
		for (int i = 0; i != 3; ++i)
		{
			if (planesEnabled[i])
			{
				int side = Partition(planes[i], bounds);

				if (side == 0)
					return -1;

				index |= (++side / 2) << i;
			}
		}

		return index;
	}
	#endregion

	#region private members - maintainence
	private Vector3 Axis(int index)
	{
		if (index == 0)
			return Vector3.right;
		else if (index == 1)
			return Vector3.up;
		else if (index == 2)
			return Vector3.forward;
		else
			throw new ArgumentOutOfRangeException("index");
	}

	private void Split()
	{
		// Find a decent splitting point for this node
		// Currently just the mean center, but could be more sophisticated
		// (e.g. weight average by inverse of radius)
		Vector3 splitPoint = Vector3.zero;

		foreach (T data in objects)
			splitPoint += data.Bounds.center;

		splitPoint /= (float)objects.Count;

		// Set up planes
		for (int i = 0; i != 3; ++i)
		{
			// -splitPoint???
			planes[i] = new Plane(Axis(i), -splitPoint[i]);
			planesEnabled[i] = true;
		}

		// Count number of straddling objects for each plane.
		int[] planesCount = new int[3] { 0, 0, 0 };

		for (int i = 0; i != 3; ++i)
		{
			foreach (T data in objects)
			{
				if (Partition(planes[i], data.Bounds) == 0)
					++planesCount[i];
			}

			// If count is above threshold, disable the plane.
			if (planesCount[i] > objects.Count / 4)
				planesEnabled[i] = false;
		}

		// Check we haven't disabled too many planes.
		int sumPlanes = planesEnabled.Count(p => p == true);

		if (sumPlanes < 2)
		{
			// If we have, reenable the ones with least straddlers.
			int minAt = Array.IndexOf(planesCount, planesCount.Min());

			for (int i = 0; i != 3; ++i)
			{
				if (i != minAt)
					planesEnabled[i] = true;
			}
		}

		// Create child nodes.
		foreach (int index in EnabledIndices)
			children[index] = new OctreeNode<T>(this);

		HasChildren = true;

		// Reinsert non-straddling objects.
		var notStraddling = objects.Where(d =>
		{
			for (int i = 0; i != 3; ++i)
			{
				if (planesEnabled[i] && Partition(planes[i], d.Bounds) == 0)
					return false;
			}
			return true;
		}
		).ToList();

		foreach (T data in notStraddling)
			objects.Remove(data);

		foreach (T data in notStraddling)
			Add(data);

		MarkAABBForUpdate(AABB);
	}

	// If children are all empty leaves, remove them. 
	// Make this node a leaf.
	private void Prune()
	{
		if (IsLeaf)
			return;

		// Ensure children are all empty leaf nodes
		foreach (OctreeNode<T> child in EnabledChildren)
		{
			if (!(child.IsLeaf && child.IsEmpty))
				return;
		}

		// Clear children
		foreach (int index in EnabledIndices)
			children[index] = null;

		HasChildren = false;

		// Disable planes
		for (int i = 0; i != 3; ++i)
			planesEnabled[i] = false;
	}
	#endregion

	public void DrawGizmo()
	{
		if (IsLeaf && IsEmpty)
			return;

		if (HasChildren)
		{
			foreach (OctreeNode<T> child in EnabledChildren)
				child.DrawGizmo();
		}

		Color oldColor = Gizmos.color;

		if (IsLeaf)
			Gizmos.color = Color.red;
		else
			Gizmos.color = Color.green;

		Gizmos.DrawWireCube(AABB.center, AABB.size);

		Gizmos.color = oldColor;
	}
}

public class Octree<T> where T : IOctreeObject<T>
{
	OctreeNode<T> root;

	public Octree()
	{
		root = new OctreeNode<T>(null);
	}

	public void Add(T octreeObject)
	{
		root.Add(octreeObject);

		octreeObject.Octree = this;
	}
	public void Add(IEnumerable<T> octreeObjects)
	{
		foreach (T octreeObject in octreeObjects)
			Add(octreeObject);
	}

	public void Remove(T octreeObject)
	{
		if (octreeObject.OctreeNode != null)
			octreeObject.OctreeNode.Remove(octreeObject);

		octreeObject.Octree = null;
	}
	public void Remove(IEnumerable<T> octreeObjects)
	{
		foreach (T octreeObject in octreeObjects)
			Remove(octreeObject);
	}

	public void Update()
	{
		root.Update();
	}

	public void Query(ICollection<T> result, Bounds bounds)
	{
		root.Query(result, bounds);
	}

	public void Query(ICollection<T> result, Plane[] planes)
	{
		root.Query(result, planes);
	}

	public void Initialize()
	{
		// Update once, remove everything and re-insert it then update again.
		// Not sure why this is necessary... 
		// probably a bug with adding objects / splitting nodes.

		Update();

		List<T> octreeObjects = new List<T>();
		root.All(octreeObjects);

		foreach (T o in octreeObjects)
		{
			o.OctreeNode.Remove(o);
			o.Octree.Add(o);
		}

		Update();
	}

	public int Count()
	{
		return root.CountNodes;
	}

	public void DrawGizmo()
	{
		root.DrawGizmo();
	}
}
