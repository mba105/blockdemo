using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class AnimationFix : MonoBehaviour
{
	private void OnEnable()
	{
		if (!animation.isPlaying)
			animation.Play();

		animation["Default Take"].time = Time.time % animation.clip.length;
	}

	private void OnDisable()
	{
		if (animation.isPlaying)
			animation.Stop();
	}
}
