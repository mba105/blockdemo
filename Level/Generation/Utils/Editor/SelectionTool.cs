using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.Linq;

public class SelectionTool : ScriptableWizard
{
	#region wizard interface
	[MenuItem("Window/Selection Tool")]
	static void CreateWizard()
	{
		ScriptableWizard.DisplayWizard<SelectionTool>("Selection Tool", "Select", "Deselect");
	}

	public string searchFor = "";

	public enum SearchBy
	{
		Type, Tag, Name,
	}

	public SearchBy searchBy = SearchBy.Tag;

	private void OnGUI()
	{
		EditorGUILayout.BeginVertical();

			searchBy = (SearchBy) EditorGUILayout.EnumPopup(
				new GUIContent("Search By", ""),
				searchBy);
			
			searchFor = EditorGUILayout.TextField(
				new GUIContent("Search For", ""),
				searchFor);
			
			GUILayout.FlexibleSpace();

			EditorGUILayout.BeginHorizontal();

				GUILayout.FlexibleSpace();

				if (GUILayout.Button(
					new GUIContent("Select", "")))
				{
					if (searchFor != "")
					{
						if (searchBy == SearchBy.Type)
						{
							SelectType(searchFor);
						}
						else if (searchBy == SearchBy.Tag)
						{
							SelectTag(searchFor);
						}
						else if (searchBy == SearchBy.Name)
						{
							SelectName(searchFor);
						}
					}
				}

				EditorGUILayout.Separator();

				if (GUILayout.Button(
					new GUIContent("Deselect", "")))
				{
					if (searchFor != "")
					{
						if (searchBy == SearchBy.Type)
						{
							DeselectType(searchFor);
						}
						else if (searchBy == SearchBy.Tag)
						{
							DeselectTag(searchFor);
						}
						else if (searchBy == SearchBy.Name)
						{
							DeselectName(searchFor);
						}
					}
				}
				
				EditorGUILayout.Separator();

				if (GUILayout.Button(
					new GUIContent("Deselect Other", "")))
				{
					if (searchFor != "")
					{
						if (searchBy == SearchBy.Type)
						{
							DeselectOtherType(searchFor);
						}
						else if (searchBy == SearchBy.Tag)
						{
							DeselectOtherTag(searchFor);
						}
						else if (searchBy == SearchBy.Name)
						{
							DeselectOtherName(searchFor);
						}
					}
				}

				EditorGUILayout.Separator();

				if (GUILayout.Button(
					new GUIContent("Deselect All", "")))
				{
					DeselectAll();
				}
				
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.Separator();

		EditorGUILayout.EndVertical();
	}
	#endregion

	#region select
	static private void SelectType(string searchString)
	{
		Type type = Types.GetType(searchString, "UnityEngine");

		if (type == null)
		{
			Debug.LogWarning("Could not find type: " + searchString);
			return;
		}

		var objects = UnityEngine.Object.FindSceneObjectsOfType(type);

		Selection.objects = Selection.objects.Concat(objects).Distinct().ToArray();
	}

	static private void SelectTag(string searchString)
	{
		GameObject[] objects;
		try
		{
			objects = GameObject.FindGameObjectsWithTag(searchString);
		}
		catch (UnityException)
		{
			Debug.LogWarning("Tag: " + searchString + " does not exist.");
			return;
		}

		Selection.objects = Selection.objects.Concat(objects).Distinct().ToArray();
	}

	static private void SelectName(string searchString)
	{
		var objects = UnityEngine.Object.FindSceneObjectsOfType(
			typeof(GameObject));
		
		Selection.objects = Selection.objects.Concat(
			objects.Where(o => o.name == searchString)).Distinct().ToArray();
	}
	#endregion

	#region deselect
	static private void DeselectType(string searchString)
	{
		Type type = Types.GetType(searchString, "UnityEngine");

		if (type == null)
		{
			Debug.LogWarning("Could not find type: " + searchString);
			return;
		}

		Selection.objects = Selection.objects.Where(o => o.GetType() != type).ToArray();
	}

	static private void DeselectTag(string searchString)
	{
		var gameObjects = Selection.GetFiltered(
			typeof(GameObject), SelectionMode.Unfiltered);

		if (gameObjects == null)
		{
			Debug.LogWarning("No GameObjects selected.");
			return;
		}

		var objects = gameObjects.Where(o => 
			{
				GameObject go = o as GameObject;

				return go != null && go.tag == searchString;
			});

		Selection.objects = Selection.objects.Except(objects).ToArray();
	}

	static private void DeselectName(string searchString)
	{
		Selection.objects = Selection.objects.Where(o => o.name != searchString).ToArray();
	}
	#endregion

	#region deselect other
	static private void DeselectOtherType(string searchString)
	{
		Type type = Types.GetType(searchString, "UnityEngine");

		if (type == null)
		{
			Debug.LogError("Could not find type: " + searchString);
			return;
		}

		Selection.objects = Selection.objects.Where(o => o.GetType() == type).ToArray();
	}

	static private void DeselectOtherTag(string searchString)
	{
		Selection.objects = Selection.objects.Where(o =>
			{
				GameObject go = o as GameObject;

				return go != null && go.tag == searchString;
			}).ToArray();
	}

	static private void DeselectOtherName(string searchString)
	{
		Selection.objects = Selection.objects.Where(o => o.name == searchString).ToArray();
	}
	#endregion

	#region deselect all
	static private void DeselectAll()
	{
		Selection.objects = new UnityEngine.Object[0];
	}
	#endregion
}
