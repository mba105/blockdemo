using System;
using System.Collections;
using System.Collections.Generic;

namespace MinElement
{
	public static class MinElement
	{
		// Same as System.Linq.Enumerable.Min, but with a custom comparison!
		// Standard Min function provides an optional selector instead, which is pretty useless.

		public static T MinElement<T>(this IEnumerable<T> enumerable)
		{
			return MinElement(enumerable, Comparer<T>.Default.Compare);
		}

		public static T MinElement<T>(this IEnumerable<T> source, Comparison<T> comparison)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}

			T min = default(T);

			if (min == null)
			{
				foreach (T e in source)
				{
					if ((e != null) && ((min == null) || (comparison(e, min) < 0)))
					{
						min = e;
					}
				}

				return min;
			}

			bool isMinSet = false;
			foreach (T e in source)
			{
				if (isMinSet)
				{
					if (comparison(e, min) < 0)
					{
						min = e;
					}
				}
				else
				{
					min = e;
					isMinSet = true;
				}
			}

			if (!isMinSet)
			{
				throw new InvalidOperationException("No elements in source.");
			}

			return min;
		}


	}
}
