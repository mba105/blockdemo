using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DLX
{
	// Knuth's Algorithm X - "Dancing Links" Implementation

	// Test Case (1 solution: B, D, F)
	//DLX.DancingLinks<char, int> dlx = new DLX.DancingLinks<char, int>();
	//int[] constraints = new int[] { 1, 2, 3, 4, 5, 6, 7 };
	//char[] choices = new char[] { 'A', 'B', 'C', 'D', 'E', 'F' };
	//int[][] choiceData = new int[][]
	//{
	//    new int[] { 1, 4, 7 },
	//    new int[] { 1, 4 },
	//    new int[] { 4, 5, 7 },
	//    new int[] { 3, 5, 6 },
	//    new int[] { 2, 3, 6, 7},
	//    new int[] { 2, 7 }
	//};
	//dlx.AddConstraints(constraints);
	//dlx.AddChoices(choices.Zip(choiceData, (ch, co) =>
	//    new DLX.DancingLinks<char, int>.ChoiceData(ch, co)).ToArray());
	//List<char[]> solutions = new List<char[]>();
	//dlx.Solve(solutions);

	// Test Case (1 solution: 4, 5, 1)
	//DLX.DancingLinks<int, char> dlx = new DLX.DancingLinks<int, char>();
	//char[] constraints = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };
	//int[] choices = new int[] { 1, 2, 3, 4, 5, 6 };
	//char[][] choiceData = new char[][]
	//{
	//    new char[] { 'C', 'E', 'F' },
	//    new char[] { 'A', 'D', 'G' },
	//    new char[] { 'B', 'C', 'F' },
	//    new char[] { 'A', 'D' },
	//    new char[] { 'B', 'G' },
	//    new char[] { 'D', 'E', 'G' },
	//};
	//dlx.AddConstraints(constraints);
	//dlx.AddChoices(choices.Zip(choiceData, (ch, co) =>
	//    new DLX.DancingLinks<int, char>.ChoiceData(ch, co)).ToArray());
	//List<int[]> solutions = new List<int[]>();
	//dlx.Solve(solutions);

	public class DancingLinks<ChoiceT, ConstraintT>
	{
		private class Node
		{
			public Node()
			{
				Row = null;
				Column = null;

				Left = Right = Up = Down = this;
			}

			public Row Row;
			public Column Column;
			public Node Left, Right, Up, Down;
		}

		private class Column : Node
		{
			public Column() { }

			public Column(ConstraintT name)
			{
				Name = name;
			}

			public ConstraintT Name;

			#region properties
			public int Count
			{
				get;
				protected set;
			}
			#endregion

			#region add
			public void AddLast(Node newNode)
			{
				ValidateNewNode(newNode);
				InsertNodeBefore(this, newNode);
				newNode.Column = this;
			}

			public void Replace(Node node)
			{
				ValidateNode(node);

				++Count;

				node.Up.Down = node;
				node.Down.Up = node;
			}
			#endregion

			#region remove
			public void Remove(Node node)
			{
				ValidateNode(node);
				RemoveNode(node);
			}
			#endregion

			#region enumerate
			public IEnumerable<Node> Downwards
			{
				get
				{
					Node node = Down;
					
					while (node != this)
					{
						yield return node;

						node = node.Down;
					}
				}
			}

			public IEnumerable<Node> Upwards
			{
				get
				{
					Node node = Up;

					while (node != this)
					{
						yield return node;

						node = node.Up;
					}
				}
			}
			#endregion

			#region private members
			private void ValidateNode(Node node)
			{
				if (node == null)
					throw new ArgumentNullException("node");

				if (node.Column != this)
					throw new InvalidOperationException("Node does not belong to this list.");
			}

			private void ValidateNewNode(Node newNode)
			{
				if (newNode == null)
					throw new ArgumentNullException("newNode");

				if (newNode.Column != null && newNode.Column != this)
					throw new InvalidOperationException("NewNode does not belong to this list.");
			}

			private void InsertNodeBefore(Node node, Node newNode)
			{
				newNode.Down = node;
				newNode.Up = node.Up;

				node.Up.Down = newNode;
				node.Up = newNode;

				++Count;
			}

			private void RemoveNode(Node node)
			{
				node.Down.Up = node.Up;
				node.Up.Down = node.Down;

				// Don't invalidate this node.

				--Count;
			}
			#endregion
		}

		private class Row
		{
			public Row(ChoiceT name)
			{
				Name = name;
			}

			public ChoiceT Name;

			#region properties
			public Node First
			{
				get;
				private set;
			}

			public Node Last
			{
				get { return First == null ? null : First.Left; }
			}
			#endregion

			#region add
			public void AddLast(Node newNode)
			{
				ValidateNewNode(newNode);

				if (First == null)
				{
					InsertNodeInEmptyList(newNode);
				}
				else
				{
					InsertNodeBefore(First, newNode);
				}
				newNode.Row = this;
			}
			#endregion

			#region enumerate
			public IEnumerable<Node> RightwardsRange(Node start, Node end)
			{
				ValidateNode(start);
				ValidateNode(end);

				Node node = start;

				while (node != end)
				{
					yield return node;

					node = node.Right;
				}
			}

			public IEnumerable<Node> LeftwardsRange(Node start, Node end)
			{
				ValidateNode(start);
				ValidateNode(end);

				Node node = start;

				while (node != end)
				{
					yield return node;

					node = node.Left;
				}
			}
			#endregion

			#region private members
			private void ValidateNode(Node node)
			{
				if (node == null)
					throw new ArgumentNullException("node");

				if (node.Row != this)
					throw new InvalidOperationException("Node does not belong to this list.");
			}

			private void ValidateNewNode(Node newNode)
			{
				if (newNode == null)
					throw new ArgumentNullException("newNode");

				if (newNode.Row != null && newNode.Row != this)
					throw new InvalidOperationException("NewNode does not belong to this list.");
			}

			private void InsertNodeBefore(Node node, Node newNode)
			{
				newNode.Right = node;
				newNode.Left = node.Left;

				node.Left.Right = newNode;
				node.Left = newNode;
			}

			private void InsertNodeInEmptyList(Node newNode)
			{
				newNode.Right = newNode;
				newNode.Left = newNode;

				First = newNode;
			}
			#endregion
		}

		private class ColumnList : Column
		{
			public ColumnList() { }

			#region add
			public void AddLast(Column newNode)
			{
				ValidateNewNode(newNode);
				InsertNodeBefore(this, newNode);
			}

			public void Replace(Column node)
			{
				ValidateNode(node);

				++Count;

				node.Right.Left = node;
				node.Left.Right = node;
			}
			#endregion

			#region remove
			public void Remove(Column node)
			{
				ValidateNode(node);
				RemoveNode(node);
			}
			#endregion

			#region search
			public Column FindFirst(ConstraintT value)
			{
				EqualityComparer<ConstraintT> c = EqualityComparer<ConstraintT>.Default;

				foreach (Column node in Forwards)
				{
					if (c.Equals(value, node.Name))
						return node;
				}

				return null;
			}

			public Column MinElement()
			{
				if (Right == this)
					return null;

				Column node = Right as Column;
				Column min = node;

				Comparer<int> c = Comparer<int>.Default;

				while (node != this)
				{
					if (c.Compare(node.Count, min.Count) < 0)
						min = node;

					node = node.Right as Column;
				}

				return min;
			}
			#endregion

			#region enumerate
			public IEnumerable<Column> Forwards
			{
				get
				{
					if (Right == this)
						yield break;

					Column node = Right as Column;

					while (node != this)
					{
						yield return node;

						node = node.Right as Column;
					}
				}
			}
			#endregion

			#region private members
			private void ValidateNode(Column node)
			{
				if (node == null)
					throw new ArgumentNullException("node");
			}

			private void ValidateNewNode(Column newNode)
			{
				if (newNode == null)
					throw new ArgumentNullException("newNode");
			}

			private void InsertNodeBefore(Column node, Column newNode)
			{
				newNode.Right = node;
				newNode.Left = node.Left;

				node.Left.Right = newNode;
				node.Left = newNode;

				++Count;
			}

			private void RemoveNode(Column node)
			{
				node.Right.Left = node.Left;
				node.Left.Right = node.Right;

				// Don't invalidate this node.

				--Count;
			}
			#endregion
		}

		public class ChoiceData
		{
			public ChoiceData(ChoiceT choice, ConstraintT[] constraints)
			{
				this.choice = choice;
				this.constraints = constraints;
			}

			public ChoiceT choice;
			public ConstraintT[] constraints;
		}

		private ColumnList columns;
		private Stack<ChoiceT> currentChoices;

		public DancingLinks()
		{
			columns = new ColumnList();
			currentChoices = new Stack<ChoiceT>();
		}

		public void AddConstraints(IEnumerable<ConstraintT> data)
		{
			foreach (ConstraintT co in data)
				columns.AddLast(new Column(co));
		}

		public void AddChoices(IEnumerable<ChoiceData> data)
		{
			foreach (ChoiceData d in data)
			{
				Row row = new Row(d.choice);

				foreach (ConstraintT co in d.constraints)
				{
					Column column = columns.FindFirst(co);

					Node node = new Node();
					
					column.AddLast(node);
					row.AddLast(node);
				}
			}
		}

		public override string ToString()
		{
			string cs = "DLX matrix by columns: ";
			cs += "{Name} ({Size}) - {Entries}\n";

			foreach (Column column in columns.Forwards)
			{
				cs += column.Name.ToString() + " ";
				cs += "(" + column.Count + ") - ";

				foreach (Node node in column.Downwards)
					cs += node.Row.Name.ToString() + ", ";

				cs += "\n";
			}

			return cs;
		}

		public ChoiceT[] SolveMin()
		{
			Stack<ChoiceT[]> solutions = new Stack<ChoiceT[]>();

			SolveMin(solutions);

			return solutions.Last();
		}

		public ICollection<ChoiceT[]> Solve()
		{
			List<ChoiceT[]> solutions = new List<ChoiceT[]>();

			Solve(solutions);

			return solutions;
		}

		// Algorithm - Solve:
		// if no constraints left to satisfy -> it's a solution
			// add copy of list of current choices to solution
		// pick a constraint (the one satisfied by fewest choices)
			// if no choices satisfy this constraint -> no solution
		// cover this constraint
		// for each choice satisfying this constraint:
			// add this choice to list of current choices
			// for each constraint this choice satisfies:
				// cover this constraint
			// recurse with current choices -> solve()
			// for all constraints this choice satisfies:
				// uncover this constraint
		// uncover this constraint
		// ...
		public void Solve(ICollection<ChoiceT[]> solutions)
		{
			if (columns.Count == 0)
			{
				solutions.Add(currentChoices.ToArray());
				return;
			}

			Column column = columns.MinElement();
			if (column.Count == 0)
				return;

			Cover(column);

			foreach (Node colNode in column.Downwards)
			{
				currentChoices.Push(colNode.Row.Name);

				foreach (Node rowNode in colNode.Row.RightwardsRange(colNode.Right, colNode))
					Cover(rowNode.Column);

				Solve(solutions);

				foreach (Node rowNode in colNode.Row.LeftwardsRange(colNode.Left, colNode))
					Uncover(rowNode.Column);

				currentChoices.Pop();
			}

			Uncover(column);
		}

		public void SolveMin(Stack<ChoiceT[]> solutions)
		{
			if (solutions.Count != 0)
			{
				if (currentChoices.Count >= solutions.Peek().Count())
					return;
			}

			if (columns.Count == 0)
			{
				if (solutions.Count != 0)
					solutions.Pop();
				solutions.Push(currentChoices.ToArray());
				return;
			}

			Column column = columns.MinElement();
			if (column.Count == 0)
				return;

			Cover(column);

			foreach (Node colNode in column.Downwards)
			{
				currentChoices.Push(colNode.Row.Name);

				foreach (Node rowNode in colNode.Row.RightwardsRange(colNode.Right, colNode))
					Cover(rowNode.Column);

				SolveMin(solutions);

				foreach (Node rowNode in colNode.Row.LeftwardsRange(colNode.Left, colNode))
					Uncover(rowNode.Column);

				currentChoices.Pop();
			}

			Uncover(column);
		}
		
		// Algorithm - Cover:
		// remove column header
		// for each item in this column
			// remove all other items in the same row from their columns
		// ...
		void Cover(Column column)
		{
			columns.Remove(column);

			foreach (Node colNode in column.Downwards)
				foreach (Node rowNode in colNode.Row.RightwardsRange(colNode.Right, colNode))
					rowNode.Column.Remove(rowNode);
		}
		
		// Algorithm - Uncover:
		// for each item in this column
			// replace all other items in the same row in their columns
		// replace column header
		// ...
		void Uncover(Column column)
		{
			foreach (Node colNode in column.Upwards)
				foreach (Node rowNode in colNode.Row.LeftwardsRange(colNode.Left, colNode))
					rowNode.Column.Replace(rowNode);

			columns.Replace(column);
		}
	}
} // DLX
