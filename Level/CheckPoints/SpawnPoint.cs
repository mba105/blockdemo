using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour
{
	public GameObject player;

	private void Start()
	{
		// TODO: add camera to scene.

		//CameraFade.CreateOneOff(Color.black, Color.clear, 3.0f, SpawnPlayer);


		SpawnPlayer();
	}

	private void SpawnPlayer()
	{
		if (player != null)
		{
			GameObject.Instantiate(player, transform.position, Quaternion.identity);
		}
	}
}
