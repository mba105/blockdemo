using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour
{
	public bool startActive = false;
	private bool pointActive = true;

	public GameObject next;
	public GameObject previous;

	private void Start()
	{
		if (startActive)
		{
			pointActive = false;
			Activate(false);
		}
		else
		{
			pointActive = true;
			Deactivate(false);
		}
	}

	public void RespawnPlayer(GameObject player)
	{
		if (pointActive)
		{
			if (player != null)
			{
				player.transform.position = transform.position;
				player.rigidbody.velocity = Vector3.zero;
			}
		}
	}


	private GameObject[] otherCheckpoints;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			Activate(this);
		}
	}

	public static void Activate(CheckPoint point)
	{
		if (point == null)
			return;

		if (!point.pointActive)
		{
			// deactivate all other checkpoints
			GameObject[] otherCheckpoints = GameObject.FindGameObjectsWithTag("CheckPoint");

			foreach (GameObject go in otherCheckpoints)
				go.GetComponent<CheckPoint>().Deactivate(false);

			// activate this checkpoint
			point.Activate(true);
		}
	}

	private Renderer cpRenderer;
	public Color colorActive;
	public Color colorInactive;

	public GameObject activateEffect;

	private void Activate(bool visibly)
	{
		if (pointActive == false)
		{
			pointActive = true;

			if (cpRenderer == null)
				cpRenderer = GetComponentInChildren<Renderer>();

			if (cpRenderer != null)
			{
				if (cpRenderer.material.HasProperty("_EmissiveColor"))
					cpRenderer.material.SetColor("_EmissiveColor", colorActive);
			}

			if (visibly)
			{
				SoundEffect.Create(activateEffect, transform.position);
			}
		}
	}

	private void Deactivate(bool visibly)
	{
		if (pointActive)
		{
			pointActive = false;

			if (cpRenderer == null)
				cpRenderer = GetComponentInChildren<Renderer>();

			if (cpRenderer != null)
			{
				if (cpRenderer.material.HasProperty("_EmissiveColor"))
					cpRenderer.material.SetColor("_EmissiveColor", colorInactive);
			}
		}
	}

	public bool IsActive
	{
		get { return pointActive; }
	}
}
