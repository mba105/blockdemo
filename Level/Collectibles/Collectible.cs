using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour
{
	public enum ScoreType { Coin, Gem, }
	public ScoreType scoreType;

	public GameObject pickupEffect;

	private void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Player")
		{
			Score score = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();
			
			if (scoreType == ScoreType.Coin)
			{
				score.Coins = score.Coins + 1;
			}
			else if (scoreType == ScoreType.Gem)
			{
				score.Gems = score.Gems + 1;
			}

			SoundEffect.Create(pickupEffect, transform.position);

			Destroy(gameObject);
		}
	}
}
