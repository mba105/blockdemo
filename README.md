# 2D Block Demo #

![image1.png](https://bitbucket.org/repo/p8BAjo/images/475732850-image1.png)

One quick and easy way of making a tile-based 2D or pseudo-2D (block-based) level for a game is to use an image editor (e.g. Photoshop) to paint a pixelated map, and generate the level geometry from it using a key.

While this works fine for small areas, it’s problematic for larger levels simply due to the number of blocks. A modestly sized 256 x 256 image, for example, contains 65536 pixels, and hence blocks. One way to reduce this number (and add visual interest) is to merge adjacent blocks of the same type.

In this demo, blocks were only merged in squares, but rectangles could also be used to further reduce geometry at a cost of extra processing time.

![image2.png](https://bitbucket.org/repo/p8BAjo/images/85115345-image2.png)

> Figure 1 – Merging pixels of similar colour (enclosed by dashed lines) into boxes (solid lines). While the first two examples are obvious, the solution for the third case is not immediately apparent.

While merging boxes seems a fairly simple task, a number of more complicated cases quickly appear (see Figure 1). What is actually needed is to find the smallest possible set of squares covering an arbitrary shape: the NP-complete minimum exact set cover problem. This demo uses Donald Knuth’s “Algorithm X” specifically the “Dancing Links” (see P159 listed [here](http://www-cs-faculty.stanford.edu/~uno/preprints.html#ref))  method to find a solution.

In order to present data to the algorithm, it’s first necessary to process the image and find all possible squares containing identical pixels (a box). Each box (set of pixels) found is used as a potential choice by the algorithm in order to fulfil the constraints (set of all pixels of that colour).

![image3.png](https://bitbucket.org/repo/p8BAjo/images/2910184316-image3.png)

> Figure 2 – All the possible boxes (right) covering pixels in the original shape (left). Every pixel in the original must be covered once by a set of boxes forming a solution. The smallest number of boxes that covers all the pixels is the desired minimum solution.

Pseudo-code for merging blocks in the demo is thus:

```
#!csharp

// <-- divide image into smaller subimages
// <-- preprocess subimage to find possible boxes
// <-- preprocess boxes into unconnected sets

// foreach unconnected set:

	DancingLinks<Box, int> dlx = new DancingLinks<Box, int>();

	// find pixels covered by each box in this set of boxes
	// width and height are image dimensions used to find pixel indices
	var coverage =
		from c in set
		select c.Coverage(width, height);

	// find every pixel covered by a box in this set
	var constraints =
		(from x in coverage
		 from y in x
		 select y).Distinct();


	// add these as the constraints of the dancing links algorithm
	// (every pixel must be covered by a given set of boxes for that set to be a solution)
	dlx.AddConstraints(constraints);

	// set up the choices of the dancing links algorithm
	// (each box is used as the key to a set of covered pixels)
	dlx.AddChoices(
		set.Zip(coverage,
			(ch, co) => new DancingLinks<Box, int>.ChoiceData(ch, co)));

	// find the smallest set of boxes that covers all the pixels
	Box[] minSolution = dlx.SolveMin();

	// add solution for this unconnected set to the full solution for the level
	fullSolution.AddRange(minSolution);
```

The same Dancing Links algorithm is used in a Sudoku puzzle solver, which can be found [here](https://bitbucket.org/thefalcon/sudoku-solver).

Unfortunately, such a brute force technique has problems: specifically the run-time of the algorithm increases rapidly with larger images. Some optimisations can be applied (e.g. sorting boxes into overlapping sets before solving, terminating searches longer than the smallest solution found so far) and level processing is done offline, but it is still impractical to process larger images (see Table 1).

Image Size (pixels) | Time to Process (ms)
-:|:-
8 x 8 | 15
12 x 12 | 58
16 x 16 | 320
20 x 20 | 1285
24 x 24 | 4200
28 x 28 | 11400

> Table 1 – Approximate processing time to find minimum solution for images of one colour (and thus highest number of possible boxes).

The solution used in the demo is simply to cut up the level image into 12 x 12 pixel sub-images before processing (adjustable to balance processing time and effectiveness). This has the additional advantage of setting maximum block size, which is necessary anyway for practical reasons (e.g. camera placement). While the exact time for level generation is highly variable based on content, it’s simple to calculate the worst case (a 12 x 12 sub-image with no colour variation).

In this demo the main level is a 768 x 480 image, of which ~37000 pixels (10%) are filled, and ~8200 blocks generated after processing.

## Demo Controls ##

Movement Keys – left and right arrows (or a / d).

Jump Key – up arrow (or w).

Cheats – [ and ] activate the previous and next checkpoint respectively. Move into a hazard to respawn at the new point.