using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class PlayerCheats : MonoBehaviour
{
	private CheckPoint[] checkpoints;

	private void Start()
	{
		GameObject[] checkPointObjects = GameObject.FindGameObjectsWithTag("CheckPoint");
		checkpoints = Array.ConvertAll(checkPointObjects, o => o.GetComponentInChildren<CheckPoint>());
	}

	// TODO: public static active variable in checkpoint script?
	private CheckPoint GetActiveCheckPoint()
	{
		CheckPoint activePoint = checkpoints.FirstOrDefault(cp => cp.IsActive);
		return activePoint;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.RightBracket))
		{
			CheckPoint activePoint = GetActiveCheckPoint();

			if (activePoint != null)
			{
				GameObject nextPointObject = activePoint.next;

				if (nextPointObject != null)
				{
					CheckPoint nextPoint = nextPointObject.GetComponentInChildren<CheckPoint>();
					CheckPoint.Activate(nextPoint);
				}
			}
		}

		if (Input.GetKeyDown(KeyCode.LeftBracket))
		{
			CheckPoint activePoint = GetActiveCheckPoint();

			if (activePoint != null)
			{
				GameObject previousPointObject = activePoint.previous;

				if (previousPointObject != null)
				{
					CheckPoint previousPoint = previousPointObject.GetComponentInChildren<CheckPoint>();
					CheckPoint.Activate(previousPoint);
				}
			}
		}
	}
}
