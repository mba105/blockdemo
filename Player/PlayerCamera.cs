using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
	void Awake()
	{
		GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");

		if (mainCamera == null)
			Debug.LogError("Scene must contain a main camera!");

		mainCamera.transform.parent = gameObject.transform;
	}
}
