using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Performance : MonoBehaviour
{
	public float activationRange = 50.0f;

	// scene objects
	Bounds queryBounds;
	HashSet<OctreeObject> inBounds;
	private Octree<OctreeObject> octree;

	// background objects
	HashSet<OctreeObject> inBgBounds;
	private Octree<OctreeObject> bgOctree;

	// queries allocated once to avoid gc
	Vector3 lastPosition;
	HashSet<OctreeObject> newQuery;
	HashSet<OctreeObject> newBgQuery;

	private void Start()
	{
		// scene
		{
			octree = new Octree<OctreeObject>();

			List<GameObject> objects = new List<GameObject>();

			objects.AddRange(GameObject.FindGameObjectsWithTag("Terrain"));
			objects.AddRange(GameObject.FindGameObjectsWithTag("Danger"));
			objects.AddRange(GameObject.FindGameObjectsWithTag("Collectible"));
			objects.AddRange(GameObject.FindGameObjectsWithTag("Bubbles"));

			var octreeObjects = objects.Where(o => o.GetComponent<OctreeObject>() != null).ToList();

			octree.Add(octreeObjects.ConvertAll(o => o.GetComponent<OctreeObject>()));
			octree.Initialize();

			foreach (GameObject go in objects)
				go.gameObject.SetActive(false);

			queryBounds = new Bounds(Vector3.zero, Vector3.one * activationRange);
			inBounds = new HashSet<OctreeObject>();
		}

		// background
		{
			bgOctree = new Octree<OctreeObject>();

			List<GameObject> objects = new List<GameObject>();

			objects.AddRange(GameObject.FindGameObjectsWithTag("Background"));

			var octreeObjects = objects.Where(o => o.GetComponent<OctreeObject>() != null).ToList();

			bgOctree.Add(octreeObjects.ConvertAll(o => o.GetComponent<OctreeObject>()));
			bgOctree.Initialize();

			foreach (GameObject go in objects)
				go.gameObject.SetActive(false);

			inBgBounds = new HashSet<OctreeObject>();
		}

		// queries
		newQuery = new HashSet<OctreeObject>();
		newBgQuery = new HashSet<OctreeObject>();

		lastPosition = transform.position;
	}

	private void Update()
	{
		// only update if player has moved > 1 block
		if (Vector3.Distance(lastPosition, transform.position) > 1.0f)
		{
			lastPosition = transform.position;

			// scene
			{
				queryBounds.center = transform.position;

				newQuery.Clear();
				octree.Query(newQuery, queryBounds);

				var movedOut = inBounds.Where(o => !newQuery.Contains(o));
				//var movedOut = inBounds.Except(newQuery);
				foreach (OctreeObject o in movedOut)
				{
					if (o != null)
						o.gameObject.SetActive(false);
				}

				var movedIn = newQuery.Where(o => !inBounds.Contains(o));
				//var movedIn = newQuery.Except(inBounds);
				foreach (OctreeObject o in movedIn)
				{
					if (o != null)
						o.gameObject.SetActive(true);
				}

				HashSet<OctreeObject> temp = newQuery;
				newQuery = inBounds;
				inBounds = temp;
			}
		}

		// background
		{
			Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);

			newBgQuery.Clear();
			bgOctree.Query(newBgQuery, planes);

			var movedOut = inBgBounds.Where(o => !newBgQuery.Contains(o));
			//var movedOut = inBgBounds.Except(newBgQuery);
			foreach (OctreeObject o in movedOut)
			{
				if (o != null)
					o.gameObject.SetActive(false);
			}

			var movedIn = newBgQuery.Where(o => !inBgBounds.Contains(o));
			//var movedIn = newBgQuery.Except(inBgBounds);
			foreach (OctreeObject o in movedIn)
			{
				if (o != null)
					o.gameObject.SetActive(true);
			}

			HashSet<OctreeObject> temp = newBgQuery;
			newBgQuery = inBgBounds;
			inBgBounds = temp;
		}
	}
}
