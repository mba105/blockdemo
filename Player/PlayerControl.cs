using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	public float jumpHeight = 1.0f;
	public float jumpDelay = 0.2f;
	private float jumpNext = 0.0f;
	public GameObject jumpEffect;
	public float speed = 5.0f;
	public float maxSpeedChange = 2.0f;

	public float maxSlopeAngle = 80.0f;
	private bool grounded = false;

	private bool bubbles = false;
	private Vector3 bubbleDirection = Vector3.up;
	public float bubbleForce = 10.0f;

	void FixedUpdate()
	{
		float targetSpeed = Input.GetAxis("Horizontal") * speed;
		float currentSpeed = rigidbody.velocity.x;

		float speedChange = Mathf.Clamp(targetSpeed - currentSpeed, -maxSpeedChange, maxSpeedChange);

		rigidbody.AddForce(Vector3.right * speedChange, ForceMode.VelocityChange);

		if (grounded)
		{
	        if (Input.GetButton("Jump") && jumpNext < Time.time)
			{
				jumpNext = Time.time + jumpDelay;

				SoundEffect.Create(jumpEffect, transform.position);

				rigidbody.AddForce(Vector3.up * CalculateJumpSpeed(), ForceMode.VelocityChange);
			}
		}

		grounded = false;

		if (bubbles)
		{
			rigidbody.AddForce(bubbleDirection * bubbleForce, ForceMode.Force);
		}

		bubbles = false;
	}

	void OnCollisionStay(Collision collision)
	{
		if (!grounded)
		{
			foreach (ContactPoint cp in collision)
			{
				if (cp.otherCollider.gameObject.tag != "Terrain")
					continue;

				float angle = Vector3.Angle(Vector3.up, cp.normal);

				if (angle < maxSlopeAngle)
				{
					grounded = true;
					break;
				}
			}
		}
	}

	void OnTriggerStay(Collider collider)
	{
		if (collider.gameObject.tag == "Bubbles")
		{
			bubbles = true;
			bubbleDirection = collider.gameObject.GetComponent<Bubbles>().direction;
		}
	}

	float CalculateJumpSpeed()
	{
		// PGE = mgh
		// KE = 1/2 m v^2
		// mgh = 1/2 m v^2
		// ...
		// v = sqrt(2gh)
		return Mathf.Sqrt(2.0f * jumpHeight * -Physics.gravity.y);
	}
}
