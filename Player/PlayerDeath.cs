using UnityEngine;
using System.Collections;

public class PlayerDeath : MonoBehaviour
{

	//public int deathScore = -30;
	public GameObject deathEffect;

	public float deathDelay = 0.2f;
	public float deathNext = 0.0f;

	//private GameObject score;
	private GameObject[] checkPoints;

	private void Start()
	{
		//score = GameObject.FindGameObjectWithTag("UI");
		checkPoints = GameObject.FindGameObjectsWithTag("CheckPoint");
	}

	public void Kill(GameObject player)
	{
		if (deathNext < Time.time)
		{
			deathNext = Time.time + deathDelay;

			SoundEffect.Create(deathEffect, player.transform.position);

			// respawn player
			foreach (GameObject go in checkPoints)
				go.GetComponent<CheckPoint>().RespawnPlayer(player);

			//score.GetComponent<Score>().AddScore(deathScore);
		}
	}
}
