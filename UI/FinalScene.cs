using UnityEngine;
using System.Collections;

public class FinalScene : MonoBehaviour
{
	public GUISkin skin;

	public GameObject coinEffect;
	public GameObject gemEffect;

	public Texture2D coinImage;
	public Texture2D gemImage;

	public RectOffset margins;

	public RectOffset headerMargins;
	public float headerHeight;

	public RectOffset footerMargins;
	public float footerHeight;

	[HideInInspector]
	public Rect HeaderArea;
	[HideInInspector]
	public Rect FooterArea;
	[HideInInspector]
	public Rect MainArea;

	private void Awake()
	{
		margins = new RectOffset(20, 20, 0, 0);

		headerMargins = new RectOffset(0, 0, 20, 20);
		headerHeight = 0;

		footerMargins = new RectOffset(0, 0, 20, 20);
		footerHeight = 36;

		HeaderArea = new Rect(
				margins.left + headerMargins.left,
				margins.top + headerMargins.top,
				Screen.width - (margins.horizontal + headerMargins.horizontal),
				headerHeight);

		FooterArea = new Rect(
				margins.left + footerMargins.left,
				Screen.height - (margins.bottom + footerMargins.bottom + footerHeight),
				Screen.width - (margins.horizontal + footerMargins.horizontal),
				footerHeight);
		MainArea = new Rect(
				margins.left,
				margins.top + headerMargins.vertical + headerHeight,
				Screen.width - margins.horizontal,
				Screen.height - (margins.vertical + headerMargins.vertical + headerHeight + footerMargins.vertical + footerHeight));
	}

	private enum State
	{
		FadingIn,
		CountingCoins,
		CountingGems,
		CountingTotal,
		AwaitExit,
	}

	private State state;

	private Score score;

	private void Start()
	{
		GameObject scoreObject = GameObject.FindGameObjectWithTag("Score");
		score = scoreObject.GetComponent<Score>();

		StartFading();
	}

	private void StartFading()
	{
		state = State.FadingIn;

		Color startColor = new Color32(49, 77, 121, 255);

		Color endColor = startColor;
		endColor.a = 0.0f;

		CameraFade.CreateOneOff(startColor, endColor, 3.0f, StartCountingCoins);
	}

	private void StartCountingCoins()
	{
		state = State.CountingCoins;

		scoreCountStartTime = Time.time;
		scoreCountEndTime = scoreCountStartTime + scoreCountTime;
	}

	private void StartCountingGems()
	{
		state = State.CountingGems;

		scoreCountStartTime = Time.time;
		scoreCountEndTime = scoreCountStartTime + scoreCountTime;
	}

	private void StartCountingTotal()
	{
		state = State.CountingTotal;

		scoreCountStartTime = Time.time;
		scoreCountEndTime = scoreCountStartTime + scoreCountTime;
	}

	private float coinDisplay = 0;
	private float gemDisplay = 0;
	private float totalDisplay = 0;

	public float scoreCountTime = 3.0f;
	private float scoreCountStartTime;
	private float scoreCountEndTime;

	private void OnGUI()
	{
		GUI.skin = skin;

		GUIStyle textStyle = skin.GetStyle("FinalScoreText");

		GUILayout.BeginArea(MainArea);

		GUILayout.BeginVertical();
		GUILayout.FlexibleSpace();

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();

			GUILayout.BeginVertical();

			GUILayout.BeginHorizontal();
			GUILayout.Label(Mathf.RoundToInt(coinDisplay).ToString(), textStyle);
			GUILayout.FlexibleSpace();
			GUILayout.Label("x", textStyle);
			GUILayout.Label(coinImage);
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			GUILayout.Label(Mathf.RoundToInt(gemDisplay).ToString(), textStyle);
			GUILayout.FlexibleSpace();
			GUILayout.Label("x", textStyle);
			GUILayout.Label(gemImage);
			GUILayout.EndHorizontal();

			GUILayout.Box("", GUILayout.ExpandWidth(true));
			
			GUILayout.Label(Mathf.RoundToInt(totalDisplay).ToString(), textStyle);
			
			GUILayout.EndVertical();
			
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();

		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();

		GUILayout.EndArea();

		if (state == State.AwaitExit)
		{
			GUILayout.BeginArea(FooterArea);
			GUILayout.BeginHorizontal();

			GUILayout.FlexibleSpace();

			GUILayout.Label("Press any key to quit...");

			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
	}

	private void Update()
	{
		if (state == State.CountingCoins)
		{
			int coinActual = score.Coins;

			if (!Mathf.Approximately(coinDisplay, coinActual))
			{
				float timeFactor = Mathf.InverseLerp(scoreCountStartTime, scoreCountEndTime, Time.time);

				coinDisplay = Mathf.Lerp(0.0f, coinActual, timeFactor);
			}
			else
			{
				SoundEffect.Create(coinEffect, Vector3.zero);
				StartCountingGems();
			}
		}
		if (state == State.CountingGems)
		{
			int gemActual = score.Gems;

			if (!Mathf.Approximately(gemDisplay, gemActual))
			{
				float timeFactor = Mathf.InverseLerp(scoreCountStartTime, scoreCountEndTime, Time.time);

				gemDisplay = Mathf.Lerp(0.0f, gemActual, timeFactor);
			}
			else
			{
				SoundEffect.Create(coinEffect, Vector3.zero);
				StartCountingTotal();
			}
		}
		if (state == State.CountingTotal)
		{
			int totalActual = score.Total;

			if (!Mathf.Approximately(totalDisplay, totalActual))
			{
				float timeFactor = Mathf.InverseLerp(scoreCountStartTime, scoreCountEndTime, Time.time);

				totalDisplay = Mathf.Lerp(0.0f, totalActual, timeFactor);
			}
			else
			{
				SoundEffect.Create(gemEffect, Vector3.zero);
				state = State.AwaitExit;
			}
		}
		if (state == State.AwaitExit)
		{
			if (Input.anyKeyDown)
				Application.Quit();
		}
	}
}
