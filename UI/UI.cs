using UnityEngine;
using System.Collections;
using System;

public class UI : MonoBehaviour
{
	public GUISkin skin;

	public Texture2D coinImage;

	public RectOffset margins;

	public RectOffset headerMargins;
	public float headerHeight;

	public RectOffset footerMargins;
	public float footerHeight;

	[HideInInspector]
	public Rect HeaderArea;
	[HideInInspector]
	public Rect FooterArea;
	[HideInInspector]
	public Rect MainArea;

	private void Awake()
	{
		margins = new RectOffset(20, 20, 0, 0);
		
		headerMargins = new RectOffset(0, 0, 20, 20);
		headerHeight = 36;

		footerMargins = new RectOffset(0, 0, 20, 20);
		footerHeight = 36;

		HeaderArea = new Rect(
				margins.left + headerMargins.left,
				margins.top + headerMargins.top,
				Screen.width - (margins.horizontal + headerMargins.horizontal),
				headerHeight);

		FooterArea = new Rect(
				margins.left + footerMargins.left,
				Screen.height - (margins.bottom + footerMargins.bottom + footerHeight),
				Screen.width - (margins.horizontal + footerMargins.horizontal),
				footerHeight);

		MainArea = new Rect(
				margins.left,
				margins.top + headerMargins.vertical + headerHeight,
				Screen.width - margins.horizontal,
				Screen.height - (margins.vertical + headerMargins.vertical + headerHeight + footerMargins.vertical + footerHeight));
	}

	private Score score;

	private void Start()
	{
		GameObject scoreObject = GameObject.FindGameObjectWithTag("Score");
		score = scoreObject.GetComponent<Score>();
	}

	private void OnGUI()
	{
		GUI.skin = skin;

		GUILayout.BeginArea(HeaderArea);

		GUILayout.BeginHorizontal();

		TimeSpan timeTaken = TimeSpan.FromSeconds(score.TimeTaken);
		GUILayout.Label(String.Format("{0:00}:{1:00}", timeTaken.Minutes, timeTaken.Seconds));

		GUILayout.FlexibleSpace();

		GUILayout.Label(score.Total.ToString());
		GUILayout.Label("x");

		if (coinImage != null)
			GUILayout.Label(coinImage);

		GUILayout.EndHorizontal();

		GUILayout.EndArea();
	}
}
