using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour
{
	public float scoreChangeSpeed = 20.0f;
	public float scorePenaltyPerSecond = 1.0f;

	private float scoreDisplay = 0.0f;
	private bool penaltyActive = true;
	private float timePenalty = 0.0f;

	public int TimePenalty
	{
		get
		{
			return (int)timePenalty;
		}
	}

	public float TimeTaken
	{
		get;
		private set;
	}

	private void Start()
	{
		DontDestroyOnLoad(this);
	}

	private void Update()
	{
		if (penaltyActive && Total > 0)
			timePenalty += Time.deltaTime * scorePenaltyPerSecond;

		TimeTaken += Time.deltaTime;

		if (!Mathf.Approximately(scoreDisplay, Total))
			scoreDisplay = Mathf.MoveTowards(scoreDisplay, Total, scoreChangeSpeed * Time.deltaTime);
	}

	private int _coins = 0;
	public int Coins
	{
		get
		{
			return _coins;
		}
		set
		{
			_coins = Mathf.Max(0, value);
		}
	}

	private int _gems = 0;
	public int Gems
	{
		get
		{
			return _gems;
		}
		set
		{
			_gems = Mathf.Max(0, value);
		}
	}

	public int Total
	{
		get
		{
			int score = Coins + Gems * 20;
			return Mathf.Max(0, score - TimePenalty);
		}
	}

	public void OnLevelWasLoaded(int levelIndex)
	{
		if (Application.loadedLevelName == "FinalScene")
			penaltyActive = false;
	}
}
