using UnityEngine;
using System.Collections;
using System;

public class CameraFade : MonoBehaviour
{
	private static readonly string oneOffObjectName = "CameraFadeOneOff";
	private static readonly string persistentObjectName = "CameraFadePersistent";

	public static void CreateOneOff(Color startColor, Color endColor, float time, Action completionAction = null)
	{
		GameObject gameObject = new GameObject(oneOffObjectName);
		CameraFade cameraFade = gameObject.AddComponent<CameraFade>();

		cameraFade.StartFade(startColor, endColor, time, false, completionAction);
		
		cameraFade.completionEvent += DestroyOneOff;
	}

	public static CameraFade CreatePersistent(Color startColor, Color endColor, float time, Action completionAction = null)
	{
		GameObject gameObject = new GameObject(persistentObjectName);
		CameraFade cameraFade = gameObject.AddComponent<CameraFade>();

		cameraFade.StartFade(startColor, endColor, time, true, completionAction);

		return cameraFade;
	}

	private static void DestroyOneOff()
	{
		GameObject gameObject = GameObject.Find(oneOffObjectName);

		if (gameObject == null)
			return;

		Destroy(gameObject);
	}

	public int depth = -99;

	private Texture2D texture;
	private GUIStyle style;

	private void Awake()
	{
		texture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
		
		style = new GUIStyle();
		style.normal.background = texture;
	}


	private bool fading = false;
	private event Action completionEvent;
	
	private bool persist;
	private Color startColor, endColor;
	private float startTime, endTime;

	public void StartFade(Color startColor, Color endColor, float time, bool persist = false, Action completionAction = null)
	{
		this.persist = persist;
		this.startColor = startColor;
		this.endColor = endColor;
		
		texture.SetPixel(0, 0, startColor);
		texture.Apply();

		startTime = Time.time;
		endTime = startTime + time;

		completionEvent += completionAction;
		
		fading = true;
	}

	private void DoFade()
	{
		float currentTime = Mathf.InverseLerp(startTime, endTime, Time.time);
		Color currentColor = Color.Lerp(startColor, endColor, currentTime);
		
		texture.SetPixel(0, 0, currentColor);
		texture.Apply();
	}

	private void EndFade()
	{
		fading = false;

		Delegate[] eventList = completionEvent.GetInvocationList();

		// Actions may be added while calling the event (i.e. starting a new fade).
		// So explicitly remove called actions instead of just clearing afterwards.
		foreach (Delegate a in eventList)
		{
			if (a != null)
			{
				a.DynamicInvoke();
				completionEvent -= (Action)a;
			}
		}
	}


	private void OnGUI()
	{
		if (fading || persist)
		{
			GUI.depth = depth;
			GUI.Label(new Rect(0.0f, 0.0f, Screen.width, Screen.height), texture, style);
		}
	}

	private void Update()
	{
		if (fading)
		{
			if (Time.time > endTime)
			{
				EndFade();
				return;
			}

			DoFade();
		}
	}
}
